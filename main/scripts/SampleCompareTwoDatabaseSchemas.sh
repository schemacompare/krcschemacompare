#!/bin/bash


export CLASSPATH=<PATH TO JDBC DRIVER>:../../../dist/SchemaCompare.jar

java krc.schemacompare.app.SchemaCompare \
	--dbDesignation1=DatabaseOne \
        --dbDriver1=org.postgresql.Driver \
        --dbUrl1=jdbc:postgresql://host1.foo.com:5432/database1 --user1=database1User --password1=database1Password --schema1=public \
        \
	--dbDesignation2=DatabaseTwo \
        --dbDriver2=org.postgresql.Driver \
        --dbUrl2=jdbc:postgresql://host2.foo.com:5432/database2 --user2=database2User --password2=database2Password --schema2=public \
        \

