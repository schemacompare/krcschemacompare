#!/bin/bash


export CLASSPATH=/Users/mal/jdbc/drivers/PostgreSQL/postgresql-42.2.5.jar:../../../dist/SchemaDumpToXML.jar

java krc.schemacompare.app.SchemaDumpToXml \
        --dbDriver=org.postgresql.Driver \
        --dbUrl=jdbc:postgresql://127.0.0.1:5432/databaseName --user=dbUser --password=dbPassword --schema=public \
	--outputXmlFile=MyDumpedDatabaseSchema.xml

