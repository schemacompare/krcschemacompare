#!/bin/bash

export CLASSPATH=<PATH TO JDBC DRIVER>:../../../dist/SchemaCompare.jar

java krc.schemacompare.app.SchemaCompare \
	--dbDesignation1=MyDatabase \
        --dbDriver1=org.postgresql.Driver \
        --dbUrl1=jdbc:postgresql://127.0.0.1:5432/databaseName --user1=dbUser --password1=dbPassword --schema1=public \
        \
	--dbDesignation2=XMLFile --inputXmlFile2=MyDumpedDatabaseSchema.xml

