package krc.schemacompare.business.domain;

import junit.framework.TestCase;

import java.sql.*;

/**
 * @author mleo@kettleriverconsulting.com
 * @version $Revision$
 * @created Aug 16, 2006 8:37:17 PM
 * @cvsid $Id$
 */
public abstract class DbTestCase extends TestCase {
    protected Connection connection1 = null;
    protected Connection connection2 = null;

    protected void setUp() throws Exception {
        super.setUp();
        connectToHSQL();
        tearDown();
        connectToHSQL();
        setupSchema();
    }

    private void connectToHSQL() throws ClassNotFoundException, SQLException {
        Class.forName("org.hsqldb.jdbcDriver");
        connection1 = DriverManager.getConnection("jdbc:hsqldb:mem:SchemaCompare1", "sa", "");
        connection2 = DriverManager.getConnection("jdbc:hsqldb:mem:SchemaCompare2", "sa", "");
    }

    protected abstract void setupSchema() throws SQLException;

    protected void executeStatements(String stmts[], int connectionNumber) throws SQLException {
        Statement statement;

        if (connectionNumber == 1) {
            statement = connection1.createStatement();
        } else if (connectionNumber == 2) {
            statement = connection2.createStatement();
        } else {
            throw new IllegalArgumentException("connectionNumber must be 1 or 2, not " + connectionNumber);
        }

        for (int i = 0; i < stmts.length; i++) {
            String stmt = stmts[i];
            statement.executeUpdate(stmt);
        }
        statement.close();
    }

    protected void executeStatementsInFile(String fileName) {
        // finish
    }

    protected void tearDown() throws Exception {
        super.tearDown();
        Statement statement = connection1.createStatement();
        statement.executeUpdate("shutdown");
        statement.close();
        connection1.close();
        connection1 = null;

        statement = connection2.createStatement();
        statement.executeUpdate("shutdown");
        statement.close();
        connection2.close();
        connection2 = null;
    }
}
