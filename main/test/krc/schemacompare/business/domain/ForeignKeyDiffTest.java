package krc.schemacompare.business.domain;

import java.sql.SQLException;

/**
 * @author mleo@kettleriverconsulting.com
 * @version $Revision$
 * @created Jul 21, 2006 11:22:58 AM
 * @cvsid $Id$
 */
public class ForeignKeyDiffTest extends DbTestCase {
    private static final int TEST_INT = 86400;
    private static final String TEST_STRING = "Mike Leo";

    protected void setupSchema() throws SQLException {
        String createTableStmt = "create table foo(bar varchar(200))";
        String insertStmt = "insert into foo values ('" + ForeignKeyDiffTest.TEST_STRING + "')";
        String stmts[] = {createTableStmt, insertStmt};
        executeStatements(stmts, 1);

        createTableStmt = "create table foo(bar integer)";
        insertStmt = "insert into foo values (" + ForeignKeyDiffTest.TEST_INT + ")";
        String stmts2[] = {createTableStmt, insertStmt};
        executeStatements(stmts2, 2);
    }

}
