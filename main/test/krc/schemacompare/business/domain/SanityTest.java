package krc.schemacompare.business.domain;

import java.sql.*;

/**
 * @author mleo@kettleriverconsulting.com
 * @version $Revision$
 * @created Jul 21, 2006 11:22:58 AM
 * @cvsid $Id$
 */
public class SanityTest extends DbTestCase {
    private static final int TEST_INT = 86400;
    private static final String TEST_STRING = "Mike Leo";

    protected void setupSchema() throws SQLException {
        String createTableStmt = "create table foo(bar varchar(200))";
        String insertStmt = "insert into foo values ('" + TEST_STRING + "')";
        String stmts[] = {createTableStmt, insertStmt};
        executeStatements(stmts, 1);

        createTableStmt = "create table foo(bar integer)";
        insertStmt = "insert into foo values (" + TEST_INT + ")";
        String stmts2[] = {createTableStmt, insertStmt};
        executeStatements(stmts2, 2);
    }

    public void testHSQLSanityCheck1() throws Exception {
        Statement statement = connection1.createStatement();
        ResultSet rs = statement.executeQuery("select * from foo");
        rs.next();
        assertEquals(TEST_STRING, rs.getString(1));
        statement.close();
    }

    public void testHSQLSanityCheck2() throws Exception {
        Statement statement = connection1.createStatement();
        ResultSet rs = statement.executeQuery("select bar from foo");
        rs.next();
        assertEquals(TEST_STRING, rs.getString("bar"));
        statement.close();
    }

    public void testHSQLSanityCheck3() throws Exception {
        Statement statement = connection2.createStatement();
        ResultSet rs = statement.executeQuery("select * from foo");
        rs.next();
        assertEquals(TEST_INT, rs.getInt(1));
        statement.close();
    }

    public void testHSQLSanityCheck4() throws Exception {
        Statement statement = connection2.createStatement();
        ResultSet rs = statement.executeQuery("select bar from foo");
        rs.next();
        assertEquals(TEST_INT, rs.getInt("bar"));
        statement.close();
    }

}
