package krc.schemacompare.app;

import jargs.gnu.CmdLineParser;

/**
 * @author mleo@kettleriverconsulting.com
 * @version $Revision$
 * @created Nov 25, 2006 11:33:52 AM
 * @cvsid $Id$
 */
public class SchemaCompareParameters {

    private static SchemaCompareParameters theParameters = new SchemaCompareParameters();

    public String dbDesignation1;
    public String dbDriver1;
    public String dbUrl1;
    public String user1;
    public String password1;
    public String schema1;

    public String inputXmlFile1;

    public String dbDesignation2;
    public String dbDriver2;
    public String dbUrl2;
    public String user2;
    public String password2;
    public String schema2;

    public String inputXmlFile2;

    public String profileName;
    public String tablesIgnore;
    public Boolean showTablesWithNoDiffs;

    public static SchemaCompareParameters getInstance() {
        return theParameters;
    }

    private SchemaCompareParameters() {
    }

    public void parseParameters(String[] args) throws SchemaCompareParametersParseException {

        CmdLineParser parser = new CmdLineParser();

        CmdLineParser.Option dbDesignation1Opt = parser.addStringOption("dbDesignation1");
        CmdLineParser.Option dbDriver1Opt = parser.addStringOption("dbDriver1");
        CmdLineParser.Option dbUrl1Opt = parser.addStringOption("dbUrl1");
        CmdLineParser.Option user1Opt = parser.addStringOption("user1");
        CmdLineParser.Option password1Opt = parser.addStringOption("password1");
        CmdLineParser.Option schema1Opt = parser.addStringOption("schema1");

        CmdLineParser.Option inputXmlFile1Opt = parser.addStringOption("inputXmlFile1");

        CmdLineParser.Option dbDesignation2Opt = parser.addStringOption("dbDesignation2");
        CmdLineParser.Option dbDriver2Opt = parser.addStringOption("dbDriver2");
        CmdLineParser.Option dbUrl2Opt = parser.addStringOption("dbUrl2");
        CmdLineParser.Option user2Opt = parser.addStringOption("user2");
        CmdLineParser.Option password2Opt = parser.addStringOption("password2");
        CmdLineParser.Option schema2Opt = parser.addStringOption("schema2");

        CmdLineParser.Option inputXmlFile2Opt = parser.addStringOption("inputXmlFile2");

        CmdLineParser.Option profileNameOpt = parser.addStringOption("profileName");
        CmdLineParser.Option ignoreSpecificTablesOpt = parser.addStringOption("tablesIgnore");
        CmdLineParser.Option showTablesWithNoDiffsOpt = parser.addBooleanOption("showTablesWithNoDiffs");

        try {
            parser.parse(args);
        }
        catch (CmdLineParser.OptionException e) {
            throw new SchemaCompareParametersParseException(e.getMessage(), e);
        }

        String[] otherArgs = parser.getRemainingArgs();

        if (otherArgs.length > 0) {
            throw new SchemaCompareParametersParseException("Excessive arguments found.");
        }

        dbDesignation1 = (String) parser.getOptionValue(dbDesignation1Opt);
        dbDriver1 = (String) parser.getOptionValue(dbDriver1Opt);
        dbUrl1 = (String) parser.getOptionValue(dbUrl1Opt);
        user1 = (String) parser.getOptionValue(user1Opt);
        password1 = (String) parser.getOptionValue(password1Opt);
        schema1 = (String) parser.getOptionValue(schema1Opt);

        inputXmlFile1 = (String) parser.getOptionValue(inputXmlFile1Opt);

        dbDesignation2 = (String) parser.getOptionValue(dbDesignation2Opt);
        dbDriver2 = (String) parser.getOptionValue(dbDriver2Opt);
        dbUrl2 = (String) parser.getOptionValue(dbUrl2Opt);
        user2 = (String) parser.getOptionValue(user2Opt);
        password2 = (String) parser.getOptionValue(password2Opt);
        schema2 = (String) parser.getOptionValue(schema2Opt);

        inputXmlFile2 = (String) parser.getOptionValue(inputXmlFile2Opt);

        profileName = (String) parser.getOptionValue(profileNameOpt, SchemaCompareProfile.GENERIC_MAP);

        showTablesWithNoDiffs = (Boolean) parser.getOptionValue(showTablesWithNoDiffsOpt, Boolean.FALSE);
        tablesIgnore = (String) parser.getOptionValue(ignoreSpecificTablesOpt);

        // Assure something is specified for the first set
        if ((inputXmlFile1 == null) &&
                (dbDriver1 == null) &&
                (dbUrl1 == null) &&
                (user1 == null) &&
                (password1 == null) &&
                (schema1 == null)) {

            throw new SchemaCompareParametersParseException("either --inputXmlFile1 or the JDBC[1] theParameters must be specified");
        }

        // Assure exclusive use of jdbc/xml occurs for both the first set
        if ((inputXmlFile1 != null) &&
                ((dbDriver1 != null) ||
                        (dbUrl1 != null) ||
                        (user1 != null) ||
                        (password1 != null) ||
                        (schema1 != null))) {

            throw new SchemaCompareParametersParseException("--inputXmlFile1 cannot be specified along with JDBC[1] theParameters");
        }

        // Assure something is specified for the second set
        if ((inputXmlFile2 == null) &&
                (dbDriver2 == null) &&
                (dbUrl2 == null) &&
                (user2 == null) &&
                (password2 == null) &&
                (schema2 == null)) {

            throw new SchemaCompareParametersParseException("either --inputXmlFile2 or the JDBC[2] theParameters must be specified");
        }

        // Assure exclusive use of jdbc/xml occurs for both the second set
        if ((inputXmlFile2 != null) &&
                ((dbDriver2 != null) ||
                        (dbUrl2 != null) ||
                        (user2 != null) ||
                        (password2 != null) ||
                        (schema2 != null))) {

            throw new SchemaCompareParametersParseException("--inputXmlFile2 cannot be specified along with JDBC[2] theParameters");
        }

        // Assure all the JDBC theParameters are specified if the XML parameter is not
        if (inputXmlFile1 == null) {
            if (dbDriver1 == null) {
                throw new SchemaCompareParametersParseException(" --dbDriver1 parameter is required");
            }

            if (dbUrl1 == null) {
                throw new SchemaCompareParametersParseException(" --dbUrl1 parameter is required");
            }

            if (user1 == null) {
                throw new SchemaCompareParametersParseException(" --user1 parameter is required");
            }

            if (password1 == null) {
                throw new SchemaCompareParametersParseException(" --password1 parameter is required");
            }

            if (schema1 == null) {
                throw new SchemaCompareParametersParseException(" --schema1 parameter is required");
            }
        }

        // Assure all the JDBC theParameters are specified if the XML parameter is not
        if (inputXmlFile2 == null) {
            if (dbDriver2 == null) {
                throw new SchemaCompareParametersParseException(" --dbDriver2 parameter is required");
            }

            if (dbUrl2 == null) {
                throw new SchemaCompareParametersParseException(" --dbUrl2 parameter is required");
            }

            if (user2 == null) {
                throw new SchemaCompareParametersParseException(" --user2 parameter is required");
            }

            if (password2 == null) {
                throw new SchemaCompareParametersParseException(" --password2 parameter is required");
            }

            if (schema2 == null) {
                throw new SchemaCompareParametersParseException(" --schema2 parameter is required");
            }
        }

        // Assure both DB designations are set
        if (dbDesignation1 == null) {
            throw new SchemaCompareParametersParseException(" --dbDesignation1 parameter is required");
        }
        if (dbDesignation2 == null) {
            throw new SchemaCompareParametersParseException(" --dbDesignation2 parameter is required");
        }

    }

    public void printUsage() {
        System.err.println(
                "\n" +
                        "Usage1: \n\tjava -jar SchemaCompare.jar [--profileName profileName] \\" +
                        "\n\t\t --dbDesignation1=designation --dbDriver1=dbDriver --dbUrl1=dbUrl\\" +
                        "\n\t\t --user1=user --password1=password --schema1=schema \\" +
                        "\n\t\t --dbDesignation2=designation --dbDriver2=dbDriver --dbUrl2=dbUrl \\" +
                        "\n\t\t --user2=user --password2=password --schema2=schema" +
                        "\n\n" +

                        "Usage2: \n\tjava -jar SchemaCompare.jar [--profileName profileName] \\" +
                        "\n\t\t --dbDesignation1=designation --dbDriver1=dbDriver --dbUrl1=dbUrl \\" +
                        "\n\t\t --user1=user --password1=password --schema1=schema \\" +
                        "\n\t\t --dbDesignation2=designation --inputXmlFile2=inputXmlFile " +
                        "\n\n" +

                        "Usage3: \n\tjava -jar SchemaCompare.jar [--profileName profileName] \\" +
                        "\n\t\t --dbDesignation1=designation --inputXmlFile1=inputXmlFile \\" +
                        "\n\t\t --dbDesignation2=designation --dbDriver2=dbDriver --dbUrl2=dbUrl \\" +
                        "\n\t\t --user2=user --password2=password --schema2=schema \\" +
                        "\n\n" +

                        "Usage4: \n\tjava -jar SchemaCompare.jar [--profileName profileName] \\" +
                        "\n\t\t --dbDesignation1=designation --inputXmlFile1=inputXmlFile \\" +
                        "\n\t\t --dbDesignation2=designation --inputXmlFile2=inputXmlFile" +
                        "\n\n" +

                        "Common parameters:" +
                        "\n\t\t [--profileName profileName]" +   
                        "\n\t\t [--showTablesWithNoDiffs]" +
                        "\n\t\t [--tablesIgnore]" +
                        "\n"
        );
    }

}
