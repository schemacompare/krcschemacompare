package krc.schemacompare.app;

import krc.schemacompare.business.domain.db.Database;
import krc.schemacompare.business.domain.dbcompare.DbDifferences;
import krc.schemacompare.business.service.*;

import java.sql.SQLException;

/**
 * @author mleo@kettleriverconsulting.com
 * @version $Revision$
 * @created Jun 18, 2006 6:51:15 PM
 * @cvsid $Id$
 */
public class SchemaCompare {

    public final static void main(String[] args) throws Exception {
        SchemaCompare prog = new SchemaCompare();
        prog.execute(args);
    }

    private void execute(String[] args) throws Exception {

        JdbcDbSourceConfig jdbcConfig1 = null;
        XmlDbSourceConfig xmlConfig1 = null;

        JdbcDbSourceConfig jdbcConfig2 = null;
        XmlDbSourceConfig xmlConfig2 = null;

        Database database1;
        Database database2;

        try {

            SchemaCompareParameters schemaCompareParameters = parseParameters(args);

            SchemaCompareProfile.getInstance().initialize(schemaCompareParameters.profileName);
            System.out.println("SchemaCompare profile: " + SchemaCompareProfile.getInstance().getProfileName());
            System.out.println();

            DatabasePopulater populater1;
            if (schemaCompareParameters.dbDriver1 != null) {
                jdbcConfig1 = new JdbcDbSourceConfig(schemaCompareParameters.dbDesignation1,
                                                     schemaCompareParameters.dbDriver1,
                                                     schemaCompareParameters.dbUrl1,
                                                     schemaCompareParameters.user1,
                                                     schemaCompareParameters.password1,
                                                     schemaCompareParameters.schema1);

                // Get the metadata for database 1 from a JDBC data source
                populater1 = new JdbcDatabasePopulater(jdbcConfig1);
                database1 = populater1.getDatabase();

            } else {
                xmlConfig1 = new XmlDbSourceConfig(schemaCompareParameters.dbDesignation1, schemaCompareParameters.inputXmlFile1);

                // Get the metadata for database 1 from an XML file
                populater1 = new XmlDatabasePopulater(xmlConfig1);
                database1 = populater1.getDatabase();
            }


            DatabasePopulater populater2;
            if (schemaCompareParameters.dbDriver2 != null) {
                jdbcConfig2 = new JdbcDbSourceConfig(schemaCompareParameters.dbDesignation2,
                                                     schemaCompareParameters.dbDriver2,
                                                     schemaCompareParameters.dbUrl2,
                                                     schemaCompareParameters.user2,
                                                     schemaCompareParameters.password2,
                                                     schemaCompareParameters.schema2);

                // Get the metadata for database 2 from a JDBC data source
                populater2 = new JdbcDatabasePopulater(jdbcConfig2);
                database2 = populater2.getDatabase();

            } else {
                xmlConfig2 = new XmlDbSourceConfig(schemaCompareParameters.dbDesignation2, schemaCompareParameters.inputXmlFile2);

                // Get the metadata for database 2 from an XML file
                populater2 = new XmlDatabasePopulater(xmlConfig2);
                database2 = populater2.getDatabase();
            }

            // Set the database info into the differences object
            final DbDifferences differences = DbDifferences.getInstance();
            differences.setDatabase(database1);
            differences.setOtherDatabase(database2);

            // Compare the databases
            database1.compareDatabases(database2);

            boolean areTheDatabasesEquivalent = !differences.hasDifferences();

            if (areTheDatabasesEquivalent) {
                System.out.println();
                System.out.println("Databases are equivalent.");
                System.out.println();
            } else {
                System.out.println();
                System.out.println("Databases have differences:");
                System.out.println();
            }
            differences.printDifferences();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (jdbcConfig1 != null) {
                jdbcConfig1.close();
            }
            if (xmlConfig1 != null) {
                xmlConfig1.close();
            }
            if (jdbcConfig2 != null) {
                jdbcConfig2.close();
            }
            if (xmlConfig2 != null) {
                xmlConfig2.close();
            }
        }

    }

    private SchemaCompareParameters parseParameters(String[] args) {
        SchemaCompareParameters schemaCompareParameters = SchemaCompareParameters.getInstance();

        try {
            schemaCompareParameters.parseParameters(args);
        } catch (SchemaCompareParametersParseException e) {
            System.err.println(e.getMessage());
            schemaCompareParameters.printUsage();
            System.exit(2);
        }
        return schemaCompareParameters;
    }

}
