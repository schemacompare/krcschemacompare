package krc.schemacompare.app;

/**
 * @author mleo@kettleriverconsulting.com
 * @version $Revision$
 * @created Nov 25, 2006 11:36:15 AM
 * @cvsid $Id$
 */
public class SchemaCompareParametersParseException extends Exception {
    public SchemaCompareParametersParseException(String string) {
        super(string); 
    }

    public SchemaCompareParametersParseException(String string, Throwable throwable) {
        super(string, throwable);
    }
}
