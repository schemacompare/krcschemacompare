package krc.schemacompare.app;

import krc.schemacompare.business.service.JdbcDbSourceConfig;
import krc.schemacompare.business.service.DatabasePopulater;
import krc.schemacompare.business.service.JdbcDatabasePopulater;
import krc.schemacompare.business.domain.db.Database;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import java.sql.SQLException;
import java.io.FileWriter;
import java.io.IOException;

import jargs.gnu.CmdLineParser;

/**
 * @author mleo@kettleriverconsulting.com
 * @version $Revision$
 * @created Jun 18, 2006 6:51:15 PM
 * @cvsid $Id$
 *
 * Example parameters:
 *
 * --dbDriver="org.postgresql.Driver" --dbUrl="jdbc:postgresql://localhost:5432/mal" --user="mal" --password="mal" --schema="public" --outputXmlFile="/tmp/xml.out"
 *
 */
public class SchemaDumpToXml {

    public final static void main(String[] args) throws Exception {
        SchemaDumpToXml prog = new SchemaDumpToXml();
        prog.execute(args);
    }

    private void execute(String[] args) throws IOException {
        JdbcDbSourceConfig config = null;
        FileWriter xmlFile = null;

        try {

            Parameters parameters = parseParameters(args);
            SchemaCompareProfile.getInstance().initialize(SchemaCompareProfile.GENERIC_MAP);

            config = new JdbcDbSourceConfig("db",
                                            parameters.dbDriver,
                                            parameters.dbUrl,
                                            parameters.user,
                                            parameters.password,
                                            parameters.schema);

            // Get the metadata for specified database and schema
            DatabasePopulater populater1 = new JdbcDatabasePopulater(config);
            Database database = populater1.getDatabase();

            // Serialize the metadata to XML
            XStream xstream = new XStream(new DomDriver());
            String xml = xstream.toXML(database);

            // Write the XML to a file
            xmlFile = new FileWriter(parameters.outputXmlFile);
            xmlFile.write(xml);

            System.out.println();
            System.out.println("Schema written to " + parameters.outputXmlFile);
            System.out.println();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (config != null) {
                config.close();
            }
            if (xmlFile != null) {
                xmlFile.close();
            }
        }
    }

    private static void printUsage() {
        System.err.println(
                "Usage: \n" +
                        "\tjava -jar SchemaDumpToXml.jar \\" +
                        "\n\t\t --dbDriver=dbDriver --dbUrl=dbUrl \\" +
                        "\n\t\t --user=user --password=password --schema=schema \\" +
                        "\n\t\t --outputXmlFile=outputXmlFile" +
                        "\n");
    }

    private Parameters parseParameters(String[] args) {

        boolean parametersOk = true;

        Parameters parameters = new Parameters();


        CmdLineParser parser = new CmdLineParser();
        CmdLineParser.Option dbDriver = parser.addStringOption("dbDriver");
        CmdLineParser.Option dbUrl = parser.addStringOption("dbUrl");
        CmdLineParser.Option user = parser.addStringOption("user");
        CmdLineParser.Option password = parser.addStringOption("password");
        CmdLineParser.Option schema = parser.addStringOption("schema");
        CmdLineParser.Option outputXmlFile = parser.addStringOption("outputXmlFile");

        try {
            parser.parse(args);
        }
        catch (CmdLineParser.OptionException e) {
            System.err.println(e.getMessage());
            printUsage();
            System.exit(2);
        }

        String[] otherArgs = parser.getRemainingArgs();
        if (otherArgs.length > 0) {
            System.err.println("Excessive arguments found.");
            parametersOk = false;
        }

        parameters.dbDriver = (String) parser.getOptionValue(dbDriver);
        if (parameters.dbDriver == null) {
            System.err.println(" --dbDriver parameter is required");
            parametersOk = false;
        }

        parameters.dbUrl = (String) parser.getOptionValue(dbUrl);
        if (parameters.dbUrl == null) {
            System.err.println(" --dbUrl parameter is required");
            parametersOk = false;
        }

        parameters.user = (String) parser.getOptionValue(user);
        if (parameters.user == null) {
            System.err.println(" --user parameter is required");
            parametersOk = false;
        }

        parameters.password = (String) parser.getOptionValue(password);
        if (parameters.password == null) {
            System.err.println(" --password parameter is required");
            parametersOk = false;
        }

        parameters.schema = (String) parser.getOptionValue(schema);
        if (parameters.schema == null) {
            System.err.println(" --schema parameter is required");
            parametersOk = false;
        }

        parameters.outputXmlFile = (String) parser.getOptionValue(outputXmlFile);
        if (parameters.outputXmlFile == null) {
            System.err.println(" --outputXmlFile parameter is required");
            parametersOk = false;
        }

        if (!parametersOk) {
            printUsage();
            System.exit(2);
        }

        return parameters;
    }

    private class Parameters {
        String dbDriver;
        String dbUrl;
        String user;
        String password;
        String schema;
        String outputXmlFile;
    }

}
