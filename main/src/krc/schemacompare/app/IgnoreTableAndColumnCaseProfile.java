package krc.schemacompare.app;

public class IgnoreTableAndColumnCaseProfile extends SchemaCompareProfile {

    public void initFeatureMap() {

        featureMap.clear();

        // Most folks don't care about the order of their columns in a table,
        // but some require proper order, especially for some Object Mapping tools.
        featureMap.put(CHECK_COLUMN_ORDINAL_POSITION.toUpperCase(), Boolean.FALSE);

        // Most people try not to create case-sensitive table/column names,
        // but occassionally somebody does.  This ignores case when comparing.
        featureMap.put(IGNORE_CASE_IN_TABLE_NAMES.toUpperCase(), Boolean.TRUE);
        featureMap.put(IGNORE_CASE_IN_COLUMN_NAMES.toUpperCase(), Boolean.TRUE);

    }

}
