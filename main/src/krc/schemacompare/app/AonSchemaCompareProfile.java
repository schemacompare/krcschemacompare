package krc.schemacompare.app;

/**
 * @author mleo@kettleriverconsulting.com
 * @version $Revision$
 * @created Jul 20, 2006 4:31:11 PM
 * @cvsid $Id$
 */
public class AonSchemaCompareProfile extends GenericSchemaCompareProfile {

    public void initFeatureMap() {
        super.initFeatureMap();

        // Turn on the SQL Server "trim spaces on foreign key names" feature
        featureMap.put(CONSIDER_NVARCHAR_AND_VARCHAR_EQUILVENT, Boolean.TRUE);


    }

}