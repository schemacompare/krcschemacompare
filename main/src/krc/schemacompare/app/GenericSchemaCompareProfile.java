package krc.schemacompare.app;

/**
 * @author mleo@kettleriverconsulting.com
 * @version $Revision$
 * @created Jul 20, 2006 4:31:11 PM
 * @cvsid $Id$
 */
public class GenericSchemaCompareProfile extends SchemaCompareProfile {

    public void initFeatureMap() {

        featureMap.clear();

        // Turn on the Oracle "trim spaces on default value" feature
        featureMap.put(ORACLE_BUG_TRIM_SPACES_ON_DEFAULTS.toUpperCase(), Boolean.TRUE);

        // Turn on the SQL Server "trim spaces on foreign key names" feature
        featureMap.put(SQLSVR_BUG_TRIM_SPACES_ON_FK_NAMES.toUpperCase(), Boolean.TRUE);

        // Most folks don't care about the order of their columns in a table,
        // but some require proper order, especially for some Object Mapping tools.
        featureMap.put(CHECK_COLUMN_ORDINAL_POSITION.toUpperCase(), Boolean.FALSE);

        // Most people try not to create case-sensitive table/column names,
        // but occassionally somebody does.  This ignores case when comparing.
        featureMap.put(IGNORE_CASE_IN_TABLE_NAMES.toUpperCase(), Boolean.FALSE);
        featureMap.put(IGNORE_CASE_IN_COLUMN_NAMES.toUpperCase(), Boolean.FALSE);

    }

}
