package krc.schemacompare.app;

import java.util.HashMap;
import java.util.Map;

/**
 * @author mleo@kettleriverconsulting.com
 * @version $Revision$
 * @created Jul 20, 2006 4:29:31 PM
 * @cvsid $Id$
 */
public class SchemaCompareProfile {

    public static final String GENERIC_MAP = "Generic";
    public static final String IGNORE_CASE_MAP = "IgnoreCase";

    public static final String ORACLE_BUG_TRIM_SPACES_ON_DEFAULTS = "ORACLE_BUG_TRIM_SPACES_ON_DEFAULTS";
    public static final String CONSIDER_NVARCHAR_AND_VARCHAR_EQUILVENT = "CONSIDER_NVARCHAR_AND_VARCHAR_EQUILVENT";
    public static final String SQLSVR_BUG_TRIM_SPACES_ON_FK_NAMES = "SQLSVR_BUG_TRIM_SPACES_ON_FK_NAMES";
    public static final String CHECK_COLUMN_ORDINAL_POSITION = "CHECK_COLUMN_ORDINAL_POSITION";
    public static final String IGNORE_CASE_IN_TABLE_NAMES = "IGNORE_CASE_IN_TABLE_NAMES";
    public static final String IGNORE_CASE_IN_COLUMN_NAMES = "IGNORE_CASE_IN_COLUMN_NAMES";

    protected static SchemaCompareProfile theOne = new SchemaCompareProfile();

    private boolean i_initialized = false;
    protected String i_profileName = "*uninitialized*";

    public void initialize(String profileName) {

        Map profileMap = new HashMap();
        profileMap.put(GENERIC_MAP, new GenericSchemaCompareProfile());
        profileMap.put(IGNORE_CASE_MAP, new IgnoreTableAndColumnCaseProfile());

        SchemaCompareProfile compareProfile = (SchemaCompareProfile) profileMap.get(profileName);
        if (compareProfile == null) {
            compareProfile = (SchemaCompareProfile) profileMap.get(GENERIC_MAP);
            System.out.println("\n *WARNING* using default schema GENERIC");
        }
        theOne = compareProfile;
        theOne.initFeatureMap();
        theOne.i_initialized = true;
        theOne.i_profileName = profileName;
    }

    public static SchemaCompareProfile getInstance() {
        return theOne;
    }

    protected Map featureMap = new HashMap();

    protected void initFeatureMap() {
    }

    protected SchemaCompareProfile() {
        initFeatureMap();
    }

    public boolean isEnabled(String feature) {
        if (!theOne.i_initialized) {
            throw new IllegalStateException("initialize() was never called ... fix it");
        }
        return Boolean.TRUE.equals(featureMap.get(feature));
    }

    public String getProfileName() {
        return theOne.i_profileName;
    }
}
