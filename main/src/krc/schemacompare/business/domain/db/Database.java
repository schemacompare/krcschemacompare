package krc.schemacompare.business.domain.db;

import krc.schemacompare.business.service.DbSourceConfig;
import krc.schemacompare.business.domain.dbcompare.DbDifferences;
import krc.schemacompare.business.domain.dbcompare.DbDifference;
import krc.schemacompare.app.SchemaCompareProfile;
import krc.schemacompare.app.SchemaCompareParameters;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.Serializable;

/**
 * @author mleo@kettleriverconsulting.com
 * @version $Revision$
 * @created Jun 18, 2006 2:02:03 PM
 * @cvsid $Id$
 */
public class Database implements Serializable {

    private Map i_tableMap = new HashMap();
    private static Pattern PATTERN = null;
    private transient DbSourceConfig i_dbSourceConfig;

    public Database(DbSourceConfig dbSourceConfig) {
        i_dbSourceConfig = dbSourceConfig;
    }

    public void setDbSourceConfig(DbSourceConfig dbSourceConfig) {
        i_dbSourceConfig = dbSourceConfig;
    }

    public void compareDatabases(Database otherDatabase) {
        DbDifferences differences = DbDifferences.getInstance();

        List tableList = null;
        List otherTableList = null;

        tableList = new ArrayList(getTableMap().values());
        Collections.sort(tableList);
        removeUnwantedTables(tableList);

        otherTableList = new ArrayList(otherDatabase.getTableMap().values());
        Collections.sort(otherTableList);
        removeUnwantedTables(otherTableList);

        ListIterator tablesIterator = tableList.listIterator();
        ListIterator otherTablesIterator = otherTableList.listIterator();

        while (tablesIterator.hasNext() && otherTablesIterator.hasNext()) {

            Table table = (Table) tablesIterator.next();
            Table otherTable = (Table) otherTablesIterator.next();

            String tableName = table.getTableName();
            String otherTableName = otherTable.getTableName();

            int compVal;
            if (ignoringTableNameCase()) {
                compVal = tableName.compareToIgnoreCase(otherTableName);
            } else {
                compVal = tableName.compareTo(otherTableName);
            }

            if (compVal < 0) {
                differences.incrementNumMissingOtherTables();
                DbDifference difference = differences.getDifference(tableName, table.getTableType());
                difference.addDifferenceText(table.formatTableType() + " '" + tableName + "' NOT FOUND in '" + otherDatabase.getDatabaseDesignation() + "'");
                otherTablesIterator.previous();
            } else if (compVal > 0) {
                DbDifference difference = differences.getDifference(otherTableName, table.getTableType());
                differences.incrementNumMissingTables();
                difference.addDifferenceText(otherTable.formatTableType() + " '" + otherTableName + "' NOT FOUND in '" + getDatabaseDesignation() + "'");
                tablesIterator.previous();
            } else {

                // OK, we have the same table in both databases, let's "deep compareDatabases"
                table.compareDetails(otherTable);
            }
        }

        while (tablesIterator.hasNext()) {
            differences.incrementNumMissingOtherTables();
            Table table = (Table) tablesIterator.next();
            String tableName = table.getTableName();
            DbDifference difference = differences.getDifference(tableName, table.getTableType());
            difference.addDifferenceText(table.formatTableType() + " '" + tableName + "' NOT FOUND in '" + otherDatabase.getDatabaseDesignation() + "'");
        }

        while (otherTablesIterator.hasNext()) {
            differences.incrementNumMissingTables();
            Table otherTable = (Table) otherTablesIterator.next();
            String otherTableName = otherTable.getTableName();
            DbDifference difference = differences.getDifference(otherTableName, otherTable.getTableType());
            difference.addDifferenceText(otherTable.formatTableType() + " '" + otherTableName + "' NOT FOUND in '" + getDatabaseDesignation() + "'");
        }

    }

    private void removeUnwantedTables(List tableList) {
        for (Iterator iterator = tableList.listIterator(); iterator.hasNext();) {

            Table table = (Table) iterator.next();
            String tableName = table.getTableName();

            if(tableIsUnwanted(tableName)){
                iterator.remove();
                System.out.println("Removed Table name " + tableName + "\n");
            }
        }
    }

    private boolean tableIsUnwanted(String tableName) {
        Boolean unwanted = false;

        SchemaCompareParameters parameters = SchemaCompareParameters.getInstance();

        if(PATTERN == null){
            PATTERN = Pattern.compile("("+ parameters.tablesIgnore + ")", Pattern.CASE_INSENSITIVE );

        }
        
        Matcher m = PATTERN.matcher(tableName);

        if (m.find()) {
            unwanted = true;
        }

        return unwanted ;
    }

    public Map getTableMap() {
        return i_tableMap;
    }

    public String getDatabaseDesignation() {
        return i_dbSourceConfig.getDatabaseDesignation();
    }

    public void addTable(Table table) {
        i_tableMap.put(table.getTableName(), table);
    }

    public int getNumberOfTables() {
        return i_tableMap.size();
    }

    private boolean ignoringTableNameCase() {
        return SchemaCompareProfile.getInstance().isEnabled(SchemaCompareProfile.IGNORE_CASE_IN_TABLE_NAMES);
    }

}
