package krc.schemacompare.business.domain.db;

import krc.schemacompare.app.SchemaCompareProfile;
import java.io.Serializable;

/**
 * @author mleo@kettleriverconsulting.com
 * @version $Revision$
 * @created Jul 19, 2006 6:48:06 AM
 * @cvsid $Id$
 */
public class Column implements Comparable, Serializable {

    Table i_table;
    String i_columnName;
    boolean i_columnNameDiffers;
    String i_typeName;
    boolean i_typeNameDiffers;
    int i_columnSize;
    boolean i_columnSizeDiffers;
    int i_decimalDigits;
    boolean i_decimalDigitsDiffers;
    int i_nullable;
    boolean i_nullableDiffers;
    String i_columnDefault;
    boolean i_columnDefaultDiffers;
    int i_ordinalPosition;
    boolean i_ordinalPositionDiffers;

    public Column(Table table,
                  String columnName,
                  String typeName,
                  int columnSize,
                  int decimalDigits,
                  int nullable,
                  String columnDefault,
                  int ordinalPosition) {
        i_table = table;
        i_columnName = columnName;
        i_typeName = typeName;
        i_columnSize = columnSize;
        i_decimalDigits = decimalDigits;
        i_nullable = nullable;
        i_columnDefault = columnDefault;
        i_ordinalPosition = ordinalPosition;
    }

    public String getColumnDefault() {
        return i_columnDefault;
    }

    public String getColumnName() {
        return i_columnName;
    }

    public int getColumnSize() {
        return i_columnSize;
    }

    public int getDecimalDigits() {
        return i_decimalDigits;
    }

    public int getNullable() {
        return i_nullable;
    }

    public String getTypeName() {
        return i_typeName;
    }

    public int getOrdinalPosition() {
        return i_ordinalPosition;
    }

    public boolean equals(Object obj) {

        i_columnNameDiffers = false;
        i_typeNameDiffers = false;
        i_columnSizeDiffers = false;
        i_decimalDigitsDiffers = false;
        i_nullableDiffers = false;
        i_columnDefaultDiffers = false;
        i_ordinalPositionDiffers = false;

        if (this == obj)
            return true;
        if (!(obj instanceof Column))
            return false;

        Column otherColumn = (Column) obj;
        otherColumn.i_columnNameDiffers = false;
        otherColumn.i_typeNameDiffers = false;
        otherColumn.i_columnSizeDiffers = false;
        otherColumn.i_decimalDigitsDiffers = false;
        otherColumn.i_nullableDiffers = false;
        otherColumn.i_columnDefaultDiffers = false;
        otherColumn.i_ordinalPositionDiffers = false;

        if (ignoringColumnNameCase()) {
            if (i_columnName != null ? !i_columnName.equalsIgnoreCase(otherColumn.i_columnName) : otherColumn.i_columnName != null)
            {
                i_columnNameDiffers = true;
                otherColumn.i_columnNameDiffers = true;
            }
        } else {
            if (i_columnName != null ? !i_columnName.equals(otherColumn.i_columnName) : otherColumn.i_columnName != null)
            {
                i_columnNameDiffers = true;
                otherColumn.i_columnNameDiffers = true;
            }
        }

        if (i_columnSize != otherColumn.i_columnSize) {
            i_columnSizeDiffers = true;
            otherColumn.i_columnSizeDiffers = true;
        }

        if (i_decimalDigits != otherColumn.i_decimalDigits) {
            i_decimalDigitsDiffers = true;
            otherColumn.i_decimalDigitsDiffers = true;
        }

        if (dealWithOracleTrailingSpacesOnDefaults()) {
            String otherColumnDefault = otherColumn.i_columnDefault;
            if (otherColumnDefault != null) {
                otherColumnDefault = otherColumnDefault.trim();
            }
            if (i_columnDefault != null ? !i_columnDefault.trim().equals(otherColumnDefault) : otherColumn.i_columnDefault != null)
            {
                i_columnDefaultDiffers = true;
                otherColumn.i_columnDefaultDiffers = true;
            }
        } else {
            if (i_columnDefault != null ? !i_columnDefault.equals(otherColumn.i_columnDefault) : otherColumn.i_columnDefault != null)
            {
                i_columnDefaultDiffers = true;
                otherColumn.i_columnDefaultDiffers = true;
            }
        }

        if (i_nullable != otherColumn.i_nullable) {
            i_nullableDiffers = true;
            otherColumn.i_nullableDiffers = true;
        }

        if (i_typeName != null ? !i_typeName.equals(otherColumn.i_typeName) : otherColumn.i_typeName != null)
        {
            i_typeNameDiffers = true;
            otherColumn.i_typeNameDiffers = true;
        }

        if (checkColumnOrdinalPosition()) {
            if (i_ordinalPosition != otherColumn.i_ordinalPosition) {
                i_ordinalPositionDiffers = true;
                otherColumn.i_ordinalPositionDiffers = true;
            }
        }

        if (i_columnNameDiffers ||
                i_typeNameDiffers ||
                i_columnSizeDiffers ||
                i_decimalDigitsDiffers ||
                i_nullableDiffers ||
                i_columnDefaultDiffers ||
                i_ordinalPositionDiffers) {
            return false;
        } else {
            return true;
        }
    }


    public String toString() {
        StringBuffer value = new StringBuffer();
        value.append("\n\t\t").append(i_columnNameDiffers ? "*" : " ").append("Column Name      : '");
        value.append(i_columnName);
        value.append("'\n\t\t").append(i_typeNameDiffers ? "*" : " ").append("Column Type      : '");
        value.append(i_typeName);
        value.append("'\n\t\t").append(i_columnSizeDiffers ? "*" : " ").append("Column Size      : '");
        value.append(i_columnSize);
        value.append("'\n\t\t").append(i_decimalDigitsDiffers ? "*" : " ").append("Column Digits    : '");
        value.append(i_decimalDigits);
        value.append("'\n\t\t").append(i_columnDefaultDiffers ? "*" : " ").append("Column Default   : ");
        value.append(i_columnDefault == null ? "<NULL>" : "'" + i_columnDefault + "'");
        value.append("\n\t\t").append(i_nullableDiffers ? "*" : " ").append("Column Nullable  : '");
        if (i_nullable == java.sql.DatabaseMetaData.columnNoNulls)
            value.append("no");
        else if (i_nullable == java.sql.DatabaseMetaData.columnNullable)
            value.append("yes");
        else
            value.append("??unknown??");
        value.append("'\n\t\t").append(i_ordinalPositionDiffers ? "*" : " ").append("Ordinal Position : '");
        value.append(i_ordinalPosition);
        value.append("'");
        return value.toString();
    }

    public String getTableName() {
        return i_table.getTableName();
    }

    public Database getDatabase() {
        return i_table.getDatabase();
    }

    public int compareTo(Object object) {
        Column otherColumn = (Column) object;
        if (ignoringColumnNameCase()) {
            return i_columnName.compareToIgnoreCase(otherColumn.i_columnName);
        } else {
            return i_columnName.compareTo(otherColumn.i_columnName);
        }
    }

    private boolean ignoringColumnNameCase() {
        return SchemaCompareProfile.getInstance().isEnabled(SchemaCompareProfile.IGNORE_CASE_IN_COLUMN_NAMES);
    }

    private boolean checkColumnOrdinalPosition() {
        return SchemaCompareProfile.getInstance().isEnabled(SchemaCompareProfile.CHECK_COLUMN_ORDINAL_POSITION);
    }

    private boolean dealWithOracleTrailingSpacesOnDefaults() {
        return SchemaCompareProfile.getInstance().isEnabled(SchemaCompareProfile.ORACLE_BUG_TRIM_SPACES_ON_DEFAULTS);
    }

}
