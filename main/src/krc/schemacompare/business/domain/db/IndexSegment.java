package krc.schemacompare.business.domain.db;

import krc.schemacompare.app.SchemaCompareProfile;
import java.io.Serializable;

/**
 * @author mleo@kettleriverconsulting.com
 * @version $Revision$
 * @created Jun 18, 2006 2:02:42 PM
 * @cvsid $Id$
 */
public class IndexSegment implements Serializable, Comparable {
    private String i_tableName;
    private Short i_ordinalPosition;
    private String i_columnName;

    private Boolean i_nonNonUnique;

    public IndexSegment(String tableName,
                        Short ordinalPosition,
                        String columnName,
                        Boolean nonNonUnique) {
        this.i_columnName = columnName;
        this.i_tableName = tableName;
        this.i_ordinalPosition = ordinalPosition;
        this.i_nonNonUnique = nonNonUnique;
    }

    public String getColumnName() {
        return i_columnName;
    }

    public void setColumnName(String columnName) {
        this.i_columnName = columnName;
    }

    public String gettableName() {
        return i_tableName;
    }

    public void settableName(String tableName) {
        this.i_tableName = tableName;
    }

    public Short getOrdinalPosition() {
        return i_ordinalPosition;
    }

    public void setOrdinalPosition(Short ordinalPosition) {
        this.i_ordinalPosition = ordinalPosition;
    }

    public Boolean getNonUnique() {
        return i_nonNonUnique;
    }

    public void setNonUnique(Boolean nonNonUnique) {
        i_nonNonUnique = nonNonUnique;
    }

    public int compareTo(Object obj) {

        IndexSegment otherColumn = (IndexSegment) obj;

        int compareValue = 0;

        if (ignoringTableNameCase()) {
            if (compareValue == 0) {
                compareValue = gettableName().compareToIgnoreCase(otherColumn.gettableName());
            }
        } else {
            if (compareValue == 0) {
                compareValue = gettableName().compareTo(otherColumn.gettableName());
            }
        }

        if (compareValue == 0) {
            compareValue = getOrdinalPosition().compareTo(otherColumn.getOrdinalPosition());
        }

        if (ignoringColumnNameCase()) {
            if (compareValue == 0) {
                compareValue = getColumnName().compareToIgnoreCase(otherColumn.getColumnName());
            }
        } else {
            if (compareValue == 0) {
                compareValue = getColumnName().compareTo(otherColumn.getColumnName());
            }
        }

        if (compareValue == 0) {
            if (getNonUnique().equals(otherColumn.getNonUnique())) {
                compareValue = 0;
            } else if (Boolean.FALSE.equals(getNonUnique())) {
                compareValue = -1;
            } else {
                compareValue = 1;
            }
        }

        return compareValue;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IndexSegment)) return false;

        IndexSegment Index = (IndexSegment) o;

        if (ignoringColumnNameCase()) {
            if (!i_columnName.equalsIgnoreCase(Index.i_columnName)) return false;
        } else {
            if (!i_columnName.equals(Index.i_columnName)) return false;
        }

        if (ignoringTableNameCase()) {
            if (!i_tableName.equalsIgnoreCase(Index.i_tableName)) return false;
        } else {
            if (!i_tableName.equals(Index.i_tableName)) return false;
        }

        if (!i_ordinalPosition.equals(Index.i_ordinalPosition)) return false;

        if (!i_nonNonUnique.equals(Index.i_nonNonUnique)) return false;

        return true;
    }

    public int hashCode() {
        int result;
        result = i_tableName.hashCode();
        result = 29 * result + i_ordinalPosition.hashCode();
        result = 29 * result + i_columnName.hashCode();
        result = 29 * result + i_nonNonUnique.hashCode();
        return result;
    }

    public String toString() {
        StringBuffer value = new StringBuffer();
        value.append("\t\t\tKey Sequence        : '");
        value.append(i_ordinalPosition);
        value.append("'");
        value.append("\n\t\t\tColumn Name         : '");
        value.append(i_columnName);
        value.append("'");
        value.append("\n\t\t\tUnique?             : ");
        value.append(Boolean.TRUE.equals(i_nonNonUnique) ? "'No'" : "'Yes'");
        return value.toString();
    }

    public String toStringForIndexToString() {
        StringBuffer value = new StringBuffer();
        value.append(ignoringTableNameCase() ? i_tableName.toUpperCase() : i_tableName);
        value.append("|");
        value.append(i_ordinalPosition);
        value.append("|");
        value.append(ignoringColumnNameCase() ? i_columnName.toUpperCase() : i_columnName);
        value.append("|");
        value.append(Boolean.TRUE.equals(i_nonNonUnique) ? "'No'" : "'Yes'");
        return value.toString();
    }

    private boolean ignoringTableNameCase() {
        return SchemaCompareProfile.getInstance().isEnabled(SchemaCompareProfile.IGNORE_CASE_IN_TABLE_NAMES);
    }

    private boolean ignoringColumnNameCase() {
        return SchemaCompareProfile.getInstance().isEnabled(SchemaCompareProfile.IGNORE_CASE_IN_COLUMN_NAMES);
    }


}
