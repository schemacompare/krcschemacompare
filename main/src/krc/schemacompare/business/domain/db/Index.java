package krc.schemacompare.business.domain.db;

import java.util.Iterator;
import java.util.TreeSet;
import java.io.Serializable;

/**
 * @author mleo@kettleriverconsulting.com
 * @version $Revision$
 * @created Jun 18, 2006 2:02:42 PM
 * @cvsid $Id$
 */
public class Index implements Comparable, Serializable {

    private TreeSet indexSegments = new TreeSet();

    private String indexName;

    public Index(String indexName) {
        this.indexName = indexName;
    }

    public int compareTo(Object obj) {
        Index otherIndex = (Index) obj;
        return toString().compareTo(otherIndex.toString());
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Index)) return false;
        Index otherIndex = (Index) o;
        return toString().equals(otherIndex.toString());
    }

    public void addIndexSegment(String tableName,
                                Short ordinalPosition,
                                String columnName,
                                Boolean nonNonUnique) {
        IndexSegment indexSegment = new IndexSegment(tableName,
                                                     ordinalPosition,
                                                     columnName,
                                                     nonNonUnique);
        indexSegments.add(indexSegment);
    }

    public int hashCode() {
        return toString().hashCode();
    }

    public String toString() {
        String value = "";

        for (Iterator iterator = indexSegments.iterator(); iterator.hasNext();) {
            IndexSegment indexSegment = (IndexSegment) iterator.next();
            value += "<" + indexSegment.toStringForIndexToString() + ">";
        }

        return value.toString();
    }

    public String printFormatted() {
        String value = "\t\tIndex Name: '" + indexName + "'\n";

        boolean firstSegment = true;
        for (Iterator iterator = indexSegments.iterator(); iterator.hasNext();) {
            IndexSegment indexSegment = (IndexSegment) iterator.next();
            if (firstSegment) {
                firstSegment = false;
            } else {
                value +="\n";
            }
            value += indexSegment.toString();
        }

        return value.toString();
    }

}
