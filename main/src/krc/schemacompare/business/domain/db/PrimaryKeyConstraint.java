package krc.schemacompare.business.domain.db;

import krc.schemacompare.app.SchemaCompareProfile;
import java.io.Serializable;

/**
 * @author mleo@kettleriverconsulting.com
 * @version $Revision$
 * @created Jun 18, 2006 2:03:14 PM
 * @cvsid $Id$
 */
public class PrimaryKeyConstraint implements Comparable, Serializable {
    private String i_columnName;
    private Short i_keySequence;

    public PrimaryKeyConstraint(String columnName, Short keySequence) {
        i_columnName = columnName;
        i_keySequence = keySequence;
    }

    public String getColumnName() {
        return i_columnName;
    }

    public void setColumnName(String columnName) {
        i_columnName = columnName;
    }

    public Short getKeySequence() {
        return i_keySequence;
    }

    public void setKeySequence(Short keySequence) {
        i_keySequence = keySequence;
    }

    public int compareTo(Object obj) {

        PrimaryKeyConstraint otherColumn = (PrimaryKeyConstraint) obj;

        int compareValue = getKeySequence().compareTo(otherColumn.getKeySequence());

        if (ignoringColumnNameCase()) {
            if (compareValue == 0) {
                compareValue = getColumnName().compareToIgnoreCase(otherColumn.getColumnName());
            }
        } else {
            if (compareValue == 0) {
                compareValue = getColumnName().compareTo(otherColumn.getColumnName());
            }
        }

        return compareValue;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PrimaryKeyConstraint)) return false;

        PrimaryKeyConstraint PrimaryKeyConstraint = (PrimaryKeyConstraint) o;

        if (ignoringColumnNameCase()) {
            if (!i_columnName.equalsIgnoreCase(PrimaryKeyConstraint.i_columnName)) return false;
        } else {
            if (!i_columnName.equals(PrimaryKeyConstraint.i_columnName)) return false;
        }

        if (!i_keySequence.equals(PrimaryKeyConstraint.i_keySequence)) return false;

        return true;
    }

    public int hashCode() {
        int result;
        result = i_columnName.hashCode();
        result = 29 * result + i_keySequence.hashCode();
        return result;
    }

    public String toString() {
        StringBuffer value = new StringBuffer();
        value.append("\t\tColumn Name    : '");
        value.append(i_columnName);
        value.append("'");
        value.append("\n\t\tKey Sequence   : '");
        value.append(i_keySequence);
        value.append("'");
        return value.toString();
    }

    private boolean ignoringColumnNameCase() {
        return SchemaCompareProfile.getInstance().isEnabled(SchemaCompareProfile.IGNORE_CASE_IN_COLUMN_NAMES);
    }

}
