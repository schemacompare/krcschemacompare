package krc.schemacompare.business.domain.db;

import krc.schemacompare.app.SchemaCompareProfile;
import java.io.Serializable;

/**
 * @author mleo@kettleriverconsulting.com
 * @version $Revision$
 * @created Jun 18, 2006 2:03:33 PM
 * @cvsid $Id$
 */
public class ForeignKeyConstraint implements Comparable, Serializable {
    private String i_foreignKeyName;
    private Short i_keySequence;
    private String i_columnName;
    private String i_foreignColumnName;
    private String i_foreignTableName;
    private Short i_updateRule;
    private Short i_deleteRule;

    public ForeignKeyConstraint(String foreignKeyName,
                                Short keySequence,
                                String columnName,
                                String foreignColumnName,
                                String foreignTableName,
                                Short DeleteRule,
                                Short updateRule) {
        this.i_columnName = columnName;
        this.i_deleteRule = DeleteRule;
        this.i_foreignColumnName = foreignColumnName;
        this.i_foreignTableName = foreignTableName;
        this.i_foreignKeyName = foreignKeyName;
        this.i_keySequence = keySequence;
        this.i_updateRule = updateRule;
    }

    public String getColumnName() {
        return i_columnName;
    }

    public void setColumnName(String columnName) {
        this.i_columnName = columnName;
    }

    public Short getDeleteRule() {
        return i_deleteRule;
    }

    public void setDeleteRule(Short DeleteRule) {
        this.i_deleteRule = DeleteRule;
    }

    public String getForeignColumnName() {
        return i_foreignColumnName;
    }

    public void setForeignColumnName(String foreignColumnName) {
        this.i_foreignColumnName = foreignColumnName;
    }

    public String getForeignKeyName() {
        return i_foreignKeyName;
    }

    public void setForeignKeyName(String foreignKeyName) {
        this.i_foreignKeyName = foreignKeyName;
    }

    public String getForeignTableName() {
        return i_foreignTableName;
    }

    public void setForeignTableName(String foreignTableName) {
        this.i_foreignTableName = foreignTableName;
    }

    public Short getKeySequence() {
        return i_keySequence;
    }

    public void setKeySequence(Short keySequence) {
        this.i_keySequence = keySequence;
    }

    public Short getUpdateRule() {
        return i_updateRule;
    }

    public void setUpdateRule(Short updateRule) {
        this.i_updateRule = updateRule;
    }

    public int compareTo(Object obj) {

        ForeignKeyConstraint otherFK = (ForeignKeyConstraint) obj;

        int compareValue = 0;

        // *** todo *** Giant HACK alert ... yyz
        // This allows us to ignore the names of the foreign keys, which are sometimes system generated

//        if (dealWithSQLServerTrailingSpacesOnFKNames()) {
//            compareValue = getForeignKeyName().trim().compareTo(otherFK.getForeignKeyName().trim());
//        } else {
//            compareValue = getForeignKeyName().compareTo(otherFK.getForeignKeyName());
//        }
//
//        if (compareValue == 0) {
//            compareValue = getKeySequence().compareTo(otherFK.getKeySequence());
//        }

        if (compareValue == 0) {
            if (ignoringColumnNameCase()) {
                compareValue = getColumnName().compareToIgnoreCase(otherFK.getColumnName());
            } else {
                compareValue = getColumnName().compareTo(otherFK.getColumnName());
            }
        }

        if (compareValue == 0) {
            if (ignoringColumnNameCase()) {
                compareValue = getForeignColumnName().compareToIgnoreCase(otherFK.getForeignColumnName());
            } else {
                compareValue = getForeignColumnName().compareTo(otherFK.getForeignColumnName());
            }
        }

        if (compareValue == 0) {
            if (ignoringTableNameCase()) {
                compareValue = getForeignTableName().compareToIgnoreCase(otherFK.getForeignTableName());
            } else {
                compareValue = getForeignTableName().compareTo(otherFK.getForeignTableName());
            }
        }

        if (compareValue == 0) {
            compareValue = getUpdateRule().compareTo(otherFK.getUpdateRule());
        }

        if (compareValue == 0) {
            compareValue = getDeleteRule().compareTo(otherFK.getDeleteRule());
        }

        return compareValue;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ForeignKeyConstraint)) return false;

        ForeignKeyConstraint ForeignKeyConstraint = (ForeignKeyConstraint) o;

        if (ignoringColumnNameCase()) {
            if (!i_columnName.equalsIgnoreCase(ForeignKeyConstraint.i_columnName)) return false;
        } else {
            if (!i_columnName.equals(ForeignKeyConstraint.i_columnName)) return false;
        }

        if (!i_deleteRule.equals(ForeignKeyConstraint.i_deleteRule)) return false;

        if (ignoringColumnNameCase()) {
            if (!i_foreignColumnName.equalsIgnoreCase(ForeignKeyConstraint.i_foreignColumnName))
                return false;
        } else {
            if (!i_foreignColumnName.equals(ForeignKeyConstraint.i_foreignColumnName)) return false;
        }

        if (dealWithSQLServerTrailingSpacesOnFKNames()) {
            if (!i_foreignKeyName.trim().equals(ForeignKeyConstraint.i_foreignKeyName.trim())) return false;
        } else {
            if (!i_foreignKeyName.equals(ForeignKeyConstraint.i_foreignKeyName)) return false;
        }

        if (ignoringTableNameCase()) {
            if (!i_foreignTableName.equalsIgnoreCase(ForeignKeyConstraint.i_foreignTableName))
                return false;
        } else {
            if (!i_foreignTableName.equals(ForeignKeyConstraint.i_foreignTableName)) return false;
        }

        if (!i_keySequence.equals(ForeignKeyConstraint.i_keySequence)) return false;

        if (!i_updateRule.equals(ForeignKeyConstraint.i_updateRule)) return false;

        return true;
    }

    public int hashCode() {
        int result;
        result = i_foreignKeyName.hashCode();
        result = 29 * result + i_keySequence.hashCode();
        result = 29 * result + i_columnName.hashCode();
        result = 29 * result + i_foreignColumnName.hashCode();
        result = 29 * result + i_foreignTableName.hashCode();
        result = 29 * result + i_updateRule.hashCode();
        result = 29 * result + i_deleteRule.hashCode();
        return result;
    }

    public String toString() {
        StringBuffer value = new StringBuffer();
        value.append("\t\tForeign Key Name     : '");
        value.append(i_foreignKeyName);
        value.append("'");
        value.append("\n\t\t\tKey Sequence        : '");
        value.append(i_keySequence);
        value.append("'");
        value.append("\n\t\t\tColumn Name         : '");
        value.append(i_columnName);
        value.append("'");
        value.append("\n\t\t\tForeign Table Name  : '");
        value.append(i_foreignTableName);
        value.append("'");
        value.append("\n\t\t\tForeign Column Name : '");
        value.append(i_foreignColumnName);
        value.append("'");
        value.append("\n\t\t\tUpdate Rule         : '");
        value.append(i_updateRule);
        value.append("'");
        value.append("\n\t\t\tDelete Rule         : '");
        value.append(i_deleteRule);
        value.append("'");
        return value.toString();
    }

    private boolean ignoringTableNameCase() {
        return SchemaCompareProfile.getInstance().isEnabled(SchemaCompareProfile.IGNORE_CASE_IN_TABLE_NAMES);
    }

    private boolean ignoringColumnNameCase() {
        return SchemaCompareProfile.getInstance().isEnabled(SchemaCompareProfile.IGNORE_CASE_IN_COLUMN_NAMES);
    }

    private boolean dealWithSQLServerTrailingSpacesOnFKNames() {
        return SchemaCompareProfile.getInstance().isEnabled(SchemaCompareProfile.SQLSVR_BUG_TRIM_SPACES_ON_FK_NAMES);
    }


}
