package krc.schemacompare.business.domain.db;

import krc.schemacompare.app.SchemaCompareProfile;
import krc.schemacompare.business.domain.dbcompare.DbDifferences;
import krc.schemacompare.business.domain.dbcompare.DbDifference;

import java.util.*;
import java.io.Serializable;

/**
 * @author mleo@kettleriverconsulting.com
 * @version $Revision$
 * @created Jun 18, 2006 2:02:26 PM
 * @cvsid $Id$
 */
public class Table implements Comparable, Serializable {

    private Database i_database;
    private String i_tableName;
    private String i_tableType;

    private List columnList = new ArrayList();
    private List i_primaryKeys = new ArrayList();
    private List i_foreignKeys = new ArrayList();
    private Map i_indexes = new HashMap();

    public Table(Database database, String tableName, String tableType) {
        i_database = database;
        i_tableName = tableName;
        i_tableType = tableType;
    }

    private boolean compareColumn(Column column1, Column column2) {

        boolean retVal = column1.equals(column2);

        if (!retVal) {
            DbDifferences differences = DbDifferences.getInstance();
            differences.incrementNumColumnDiffs();

            DbDifference difference = differences.getDifference(getTableName(), getTableType());

            difference.addDifferenceText(" Column '" + column1.getColumnName() + "' in " + formatTableTypeLower() + " '" + column1.getTableName() + "' differs:");

            difference.addDifferenceText("\t" + getDatabase().getDatabaseDesignation() + ": " + column1.getTableName() + "." + column1.getColumnName() + " = " + column1.toString());
            difference.addDifferenceText("\t" + column2.getDatabase().getDatabaseDesignation() + ": " + column2.getTableName() + "." + column2.getColumnName() + " = " + column2.toString());
        }

        return retVal;

    }

    public List getColumnList() {
        return columnList;
    }

    public void addColumn(Column column) {
        columnList.add(column);
    }

    public void compareDetails(Object object) {
        Table otherTable = (Table) object;

        // See if they have the same column definitions
        compareColumns(otherTable);

        // Only do the following for tables, not views
        if (isRegularTable()) {

            // See if they have the same PRIMARY keys
            comparePrimaryKeys(otherTable);

            // See if they have the same FOREIGN keys
            compareForeignKeys(otherTable);

            // See if they have the same indexes
            compareIndexes(otherTable);

        }

    }

    public boolean isRegularTable() {
        return "TABLE".equalsIgnoreCase(getTableType());
    }

    public String formatTableType() {
        if ("TABLE".equalsIgnoreCase(getTableType())) {
            return "Table";
        } else if ("VIEW".equalsIgnoreCase(getTableType())) {
            return "View";
        } else {
            return "OddTableOrView";
        }
    }

    public String formatTableTypeLower() {
        return formatTableType().toLowerCase();
    }

    private void compareColumns(Table otherTable) {

        List columns1 = getColumnList();
        List columns2 = otherTable.getColumnList();

        Collections.sort(columns1);
        Collections.sort(columns2);

        ListIterator columns1Iterator = columns1.listIterator();
        ListIterator columns2Iterator = columns2.listIterator();

        DbDifferences differences = DbDifferences.getInstance();
        DbDifference difference = differences.getDifference(getTableName(), getTableType());

        while (columns1Iterator.hasNext() && columns2Iterator.hasNext()) {
            Column Column1 = (Column) columns1Iterator.next();
            Column Column2 = (Column) columns2Iterator.next();
            String columnName1 = Column1.getColumnName();
            String columnName2 = Column2.getColumnName();
            int compVal;

            if (ignoringColumnNameCase()) {
                compVal = columnName1.compareToIgnoreCase(columnName2);
            } else {
                compVal = columnName1.compareTo(columnName2);
            }

            if (compVal < 0) {
                differences.incrementNumMissingOtherColumns();
                difference.addDifferenceText("  Column '" + columnName1 + "' not found in " + otherTable.formatTableTypeLower() + " '" + otherTable.getDatabaseDesignation() + "." + otherTable.getTableName() + "'");
                columns2Iterator.previous();
            } else if (compVal > 0) {
                differences.incrementNumMissingColumns();
                difference.addDifferenceText("  Column '" + columnName2 + "' not found in " + formatTableTypeLower() + " '" + getDatabaseDesignation() + "." + getTableName() + "'");
                columns1Iterator.previous();
            } else {
                compareColumn(Column1, Column2);
            }
        }

        while (columns1Iterator.hasNext()) {
            Column Column1 = (Column) columns1Iterator.next();
            String columnName1 = Column1.getColumnName();
            differences.incrementNumMissingOtherColumns();
            difference.addDifferenceText("  Column '" + columnName1 + "' not found in " + otherTable.formatTableTypeLower() + " '" + otherTable.getDatabaseDesignation() + "." + otherTable.getTableName() + "'");
        }

        while (columns2Iterator.hasNext()) {
            Column Column2 = (Column) columns2Iterator.next();
            String columnName2 = Column2.getColumnName();
            differences.incrementNumMissingColumns();
            difference.addDifferenceText("  Column '" + columnName2 + "' not found in " + formatTableTypeLower() + " '" + getDatabaseDesignation() + "." + getTableName() + "'");
        }

    }

    private boolean comparePrimaryKeys(Table otherTable) {

        boolean isEquivalent = true;

        List primaryKeys1 = getPrimaryKeys();
        List primaryKeys2 = otherTable.getPrimaryKeys();

        Collections.sort(primaryKeys1);
        Collections.sort(primaryKeys2);

        Iterator primaryKeys1Iterator;
        Iterator primaryKeys2Iterator;

        if (primaryKeys1.size() != primaryKeys2.size()) {
            isEquivalent = false;
        } else {

            primaryKeys1Iterator = primaryKeys1.iterator();
            primaryKeys2Iterator = primaryKeys2.iterator();

            while (primaryKeys1Iterator.hasNext() && primaryKeys2Iterator.hasNext()) {
                PrimaryKeyConstraint PrimaryKeyConstraint1 = (PrimaryKeyConstraint) primaryKeys1Iterator.next();
                PrimaryKeyConstraint PrimaryKeyConstraint2 = (PrimaryKeyConstraint) primaryKeys2Iterator.next();
                int compVal = PrimaryKeyConstraint1.compareTo(PrimaryKeyConstraint2);
                if (compVal != 0) {
                    isEquivalent = false;
                    break;
                }
            }
        }

        if (!isEquivalent) {
            DbDifferences differences = DbDifferences.getInstance();
            DbDifference difference = differences.getDifference(getTableName(), getTableType());

            differences.incrementNumPrimaryKeyDiffs();
            difference.addDifferenceText("  The primary key of table '" + getPrintableTableName() + "':");
            primaryKeys1Iterator = primaryKeys1.iterator();
            if (!primaryKeys1Iterator.hasNext()) {
                difference.addDifferenceText("\t\t*None*");
            }
            for (; primaryKeys1Iterator.hasNext();) {
                PrimaryKeyConstraint PrimaryKeyConstraint = (PrimaryKeyConstraint) primaryKeys1Iterator.next();
                difference.addDifferenceText(PrimaryKeyConstraint.toString());
            }

            difference.addDifferenceText("  differs from that of table '" + otherTable.getPrintableTableName() + "':");
            primaryKeys2Iterator = primaryKeys2.iterator();
            if (!primaryKeys2Iterator.hasNext()) {
                difference.addDifferenceText("\t\t*None*");
            }
            for (; primaryKeys2Iterator.hasNext();) {
                PrimaryKeyConstraint PrimaryKeyConstraint = (PrimaryKeyConstraint) primaryKeys2Iterator.next();
                difference.addDifferenceText(PrimaryKeyConstraint.toString());
            }
        }

        return isEquivalent;
    }

    private boolean compareForeignKeys(Table otherTable) {

        boolean isEquivalent = true;

        List foreignKeys1 = null;
        List foreignKeys2 = null;

        foreignKeys1 = getForeignKeys();
        Collections.sort(foreignKeys1);
        foreignKeys2 = otherTable.getForeignKeys();
        Collections.sort(foreignKeys2);

        Iterator foreignKeyColumns1Iterator;
        Iterator foreignKeyColumns2Iterator;

        if (foreignKeys1.size() != foreignKeys2.size()) {
            isEquivalent = false;
        } else {

            foreignKeyColumns1Iterator = foreignKeys1.iterator();
            foreignKeyColumns2Iterator = foreignKeys2.iterator();

            while (foreignKeyColumns1Iterator.hasNext() && foreignKeyColumns2Iterator.hasNext()) {
                ForeignKeyConstraint foreignKeyConstraint1 = (ForeignKeyConstraint) foreignKeyColumns1Iterator.next();
                ForeignKeyConstraint foreignKeyConstraint2 = (ForeignKeyConstraint) foreignKeyColumns2Iterator.next();
                int compVal = foreignKeyConstraint1.compareTo(foreignKeyConstraint2);
                if (compVal != 0) {
                    isEquivalent = false;
                    break;
                }
            }
        }

        if (!isEquivalent) {
            DbDifferences differences = DbDifferences.getInstance();
            DbDifference difference = differences.getDifference(getTableName(), getTableType());

            differences.incrementNumForeignKeyDiffs();

            difference.addDifferenceText("  The foreign keys of table '" + getPrintableTableName() + "':");
            Iterator foreignKeys1Iterator = foreignKeys1.iterator();
            if (!foreignKeys1Iterator.hasNext()) {
                difference.addDifferenceText("\t\t*None*");
            }
            for (; foreignKeys1Iterator.hasNext();) {
                ForeignKeyConstraint foreignKeyConstraint = (ForeignKeyConstraint) foreignKeys1Iterator.next();
                difference.addDifferenceText(foreignKeyConstraint.toString());
            }

            difference.addDifferenceText("  differs from those of table '" + otherTable.getPrintableTableName() + "':");
            Iterator foreignKeys2Iterator = foreignKeys2.iterator();
            if (!foreignKeys2Iterator.hasNext()) {
                difference.addDifferenceText("\t\t*None*");
            }
            for (; foreignKeys2Iterator.hasNext();) {
                ForeignKeyConstraint foreignKeyConstraint = (ForeignKeyConstraint) foreignKeys2Iterator.next();
                difference.addDifferenceText(foreignKeyConstraint.toString());
            }
        }

        return isEquivalent;

    }

    private boolean compareIndexes(Table otherTable) {

        boolean isEquivalent = true;

        List indexes1 = null;
        List indexes2 = null;

        indexes1 = getIndexes();
        Collections.sort(indexes1);
        indexes2 = otherTable.getIndexes();
        Collections.sort(indexes2);

        Iterator indexColumns1Iterator;
        Iterator indexColumns2Iterator;

        if (indexes1.size() != indexes2.size()) {
            isEquivalent = false;
        } else {

            indexColumns1Iterator = indexes1.iterator();
            indexColumns2Iterator = indexes2.iterator();

            while (indexColumns1Iterator.hasNext() && indexColumns2Iterator.hasNext()) {
                Index Index1 = (Index) indexColumns1Iterator.next();
                Index Index2 = (Index) indexColumns2Iterator.next();
                int compVal = Index1.compareTo(Index2);
                if (compVal != 0) {
                    isEquivalent = false;
                    break;
                }
            }
        }

        if (!isEquivalent) {
            DbDifferences differences = DbDifferences.getInstance();
            DbDifference difference = differences.getDifference(getTableName(), getTableType());

            differences.incrementNumIndexDiffs();

            difference.addDifferenceText("  The indexes of table '" + getPrintableTableName() + "':");
            Iterator index1Iterator = indexes1.iterator();
            if (!index1Iterator.hasNext()) {
                difference.addDifferenceText("\t\t*None*");
            }
            for (; index1Iterator.hasNext();) {
                Index Index = (Index) index1Iterator.next();
                difference.addDifferenceText(Index.printFormatted());
            }

            difference.addDifferenceText("  differ from those of table '" + otherTable.getPrintableTableName() + "':");
            Iterator index2Iterator = indexes2.iterator();
            if (!index2Iterator.hasNext()) {
                difference.addDifferenceText("\t\t*None*");
            }
            for (; index2Iterator.hasNext();) {
                Index Index = (Index) index2Iterator.next();
                difference.addDifferenceText(Index.printFormatted());
            }
        }

        return isEquivalent;

    }


    public Database getDatabase() {
        return i_database;
    }

    public String getDatabaseDesignation() {
        return i_database.getDatabaseDesignation();
    }

    public String getTableName() {
        return i_tableName;
    }

    public String getPrintableTableName() {
        return getDatabaseDesignation() + "." + getTableName();
    }

    public String getTableType() {
        return i_tableType;
    }

    public List getPrimaryKeys() {
        return i_primaryKeys;
    }

    public int compareTo(Object object) {
        Table otherTable = (Table) object;
        if (ignoringTableNameCase()) {
            return i_tableName.compareToIgnoreCase(otherTable.i_tableName);
        } else {
            return i_tableName.compareTo(otherTable.i_tableName);
        }
    }

    private boolean ignoringTableNameCase() {
        return SchemaCompareProfile.getInstance().isEnabled(SchemaCompareProfile.IGNORE_CASE_IN_TABLE_NAMES);
    }

    private boolean ignoringColumnNameCase() {
        return SchemaCompareProfile.getInstance().isEnabled(SchemaCompareProfile.IGNORE_CASE_IN_COLUMN_NAMES);
    }

    public void addPrimaryKey(PrimaryKeyConstraint primaryKeyConstraint) {
        i_primaryKeys.add(primaryKeyConstraint);
    }

    public List getForeignKeys() {
        return i_foreignKeys;
    }

    public void addForeignKeyConstraint(ForeignKeyConstraint foreignKeyConstraint) {
        i_foreignKeys.add(foreignKeyConstraint);
    }

    public List getIndexes() {
        return new ArrayList(i_indexes.values());
    }

    public void addIndexSegment(String indexName,
                                String tableName,
                                Short ordinalPosition,
                                String columnName,
                                Boolean nonNonUnique) {
        Index index = (Index) i_indexes.get(indexName);
        if (index == null) {
            index = new Index(indexName);
            i_indexes.put(indexName, index);
        }
        index.addIndexSegment(tableName,
                              ordinalPosition,
                              columnName,
                              nonNonUnique);
    }
}
