package krc.schemacompare.business.domain.dbcompare;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author mleo@kettleriverconsulting.com
 * @version $Revision$
 * @created Nov 2, 2006 6:10:23 PM
 * @cvsid $Id$
 */
public class DbDifference {

    String itemType;
    String itemName;
    List differenceText = new ArrayList();

    public DbDifference(String itemType, String itemName) {
        this.itemType = itemType;
        this.itemName = itemName;
    }

    public DbDifference(String itemType, String itemName, String differenceText) {
        this.itemType = itemType;
        this.itemName = itemName;
        this.differenceText.add(differenceText);
    }

    public void addDifferenceText(String moreDifferenceText) {
        differenceText.add(moreDifferenceText);
    }

    public void printDifference() {
        for (Iterator differenceTextIterator = differenceText.iterator(); differenceTextIterator.hasNext();)
        {
            System.out.println();
            String differenceText = (String) differenceTextIterator.next();
            System.out.println(differenceText);
        }
    }

    public boolean hasDifferences() {
        return differenceText.size() > 0;
    }
}
