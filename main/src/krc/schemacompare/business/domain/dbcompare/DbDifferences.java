package krc.schemacompare.business.domain.dbcompare;

import krc.schemacompare.business.domain.db.Database;
import krc.schemacompare.app.SchemaCompareParameters;

import java.util.*;

/**
 * @author mleo@kettleriverconsulting.com
 * @version $Revision$
 * @created Nov 2, 2006 6:06:17 PM
 * @cvsid $Id$
 */
public class DbDifferences {
    private static final String EIGHTY_EQUALS_SEPARATOR = "================================================================================";

    private static DbDifferences dbDifferences = null;

    private Map tables = new HashMap();

    private Database database;
    private Database otherDatabase;

    private int numSchemaDiffs = 0;

    private int numMissingOtherTables = 0;
    private int numMissingTables = 0;

    private int numColumnDiffs = 0;
    private int numMissingOtherColumns = 0;
    private int numMissingColumns = 0;

    private int numPrimaryKeyDiffs = 0;
    private int numMissingOtherPrimaryKeys = 0;
    private int numMissingPrimaryKeys = 0;

    private int numForeignKeyDiffs = 0;
    private int numMissingOtherForeignKeys = 0;
    private int numMissingForeignKeys = 0;

    private int numIndexDiffs = 0;
    private int numMissingOtherIndexes = 0;
    private int numMissingIndexes = 0;

    public static DbDifferences getInstance() {
        if (dbDifferences == null) {
            dbDifferences = new DbDifferences();
        }
        return dbDifferences;
    }

    private DbDifferences() {
    }

    public boolean hasDifferences() {
        return numSchemaDiffs > 0;
    }

    public void printDifferences() {

        System.out.println(leftPad(10, Integer.toString(getNumSchemaDiffs())) + " schema differences between " + database.getDatabaseDesignation() + " and " + otherDatabase.getDatabaseDesignation());

        System.out.println();
        System.out.println(leftPad(10, Integer.toString(getNumMissingTables())) + " tables missing in " + database.getDatabaseDesignation());
        System.out.println(leftPad(10, Integer.toString(getNumMissingOtherTables())) + " tables missing in " + otherDatabase.getDatabaseDesignation());

        System.out.println();
        System.out.println(leftPad(10, Integer.toString(getNumMissingColumns())) + " columns missing in " + database.getDatabaseDesignation());
        System.out.println(leftPad(10, Integer.toString(getNumMissingOtherColumns())) + " columns missing in " + otherDatabase.getDatabaseDesignation());
        System.out.println(leftPad(10, Integer.toString(getNumColumnDiffs())) + " columns with differences");

        System.out.println();
//        System.out.println(leftPad(10, Integer.toString(getNumMissingPrimaryKeys() + " primary keys missing in " + database.getDatabaseDesignation());
//        System.out.println(leftPad(10, Integer.toString(getNumMissingOtherPrimaryKeys() + " primary keys missing in " + otherDatabase.getDatabaseDesignation());
        System.out.println(leftPad(10, Integer.toString(getNumPrimaryKeyDiffs())) + " primary keys with differences");

        System.out.println();
//        System.out.println(leftPad(10, Integer.toString(getNumMissingForeignKeys() + " foreign keys missing in " + database.getDatabaseDesignation());
//        System.out.println(leftPad(10, Integer.toString(getNumMissingOtherForeignKeys() + " foreign keys missing in " + otherDatabase.getDatabaseDesignation());
        System.out.println(leftPad(10, Integer.toString(getNumForeignKeyDiffs())) + " foreign keys with differences");

        System.out.println();
//        System.out.println(leftPad(10, Integer.toString(getNumMissingIndexes() + " indexes missing in " + database.getDatabaseDesignation());
//        System.out.println(leftPad(10, Integer.toString(getNumMissingOtherIndexes() + " indexes missing in " + otherDatabase.getDatabaseDesignation());
        System.out.println(leftPad(10, Integer.toString(getNumIndexDiffs())) + " indexes with differences");

        System.out.println();

        SchemaCompareParameters parameters = SchemaCompareParameters.getInstance();
        System.out.println(EIGHTY_EQUALS_SEPARATOR);
        for (Iterator differencesIterator = tables.values().iterator(); differencesIterator.hasNext();) {
            DbDifference dbDifference = (DbDifference) differencesIterator.next();
            if (dbDifference.hasDifferences() || parameters.showTablesWithNoDiffs) {
                System.out.print(dbDifference.itemType + " " + dbDifference.itemName);
                System.out.println(dbDifference.hasDifferences() ? " has differences:" : " has no differences.");
                dbDifference.printDifference();
                System.out.println(EIGHTY_EQUALS_SEPARATOR);
            }
        }
    }

    private String leftPad(int i, String text) {
        if (text != null) {
            while (text.length() < i) {
                text = " " + text;
            }
        }
        return text;
    }

    public int getNumSchemaDiffs() {
        return numSchemaDiffs;
    }

    public void setNumSchemaDiffs(int numSchemaDiffs) {
        this.numSchemaDiffs = numSchemaDiffs;
    }

    public int getNumMissingOtherTables() {
        return numMissingOtherTables;
    }

    public void incrementNumMissingOtherTables() {
        numMissingOtherTables++;
        numSchemaDiffs++;
    }

    public int getNumMissingTables() {
        return numMissingTables;
    }

    public void incrementNumMissingTables() {
        numMissingTables++;
        numSchemaDiffs++;
    }

    public int getNumColumnDiffs() {
        return numColumnDiffs;
    }

    public void incrementNumColumnDiffs() {
        numColumnDiffs++;
        numSchemaDiffs++;
    }

    public int getNumMissingOtherColumns() {
        return numMissingOtherColumns;
    }

    public void incrementNumMissingOtherColumns() {
        numMissingOtherColumns++;
        numSchemaDiffs++;
    }

    public int getNumMissingColumns() {
        return numMissingColumns;
    }

    public void incrementNumMissingColumns() {
        numMissingColumns++;
        numSchemaDiffs++;
    }

    public int getNumPrimaryKeyDiffs() {
        return numPrimaryKeyDiffs;
    }

    public void incrementNumPrimaryKeyDiffs() {
        numPrimaryKeyDiffs++;
        numSchemaDiffs++;
    }

    public int getNumMissingOtherPrimaryKeys() {
        return numMissingOtherPrimaryKeys;
    }

    public void incrementNumMissingOtherPrimaryKeys() {
        numMissingOtherPrimaryKeys++;
        numSchemaDiffs++;
    }

    public int getNumMissingPrimaryKeys() {
        return numMissingPrimaryKeys;
    }

    public void incrementNumMissingPrimaryKeys() {
        numMissingPrimaryKeys++;
        numSchemaDiffs++;
    }

    public int getNumForeignKeyDiffs() {
        return numForeignKeyDiffs;
    }

    public void incrementNumForeignKeyDiffs() {
        numForeignKeyDiffs++;
        numSchemaDiffs++;
    }

    public int getNumMissingOtherForeignKeys() {
        return numMissingOtherForeignKeys;
    }

    public void incrementNumMissingOtherForeignKeys() {
        numMissingOtherForeignKeys++;
        numSchemaDiffs++;
    }

    public int getNumMissingForeignKeys() {
        return numMissingForeignKeys;
    }

    public void incrementNumMissingForeignKeys() {
        numMissingForeignKeys++;
        numSchemaDiffs++;
    }

    public int getNumIndexDiffs() {
        return numIndexDiffs;
    }

    public void incrementNumIndexDiffs() {
        numIndexDiffs++;
        numSchemaDiffs++;
    }

    public int getNumMissingOtherIndexes() {
        return numMissingOtherIndexes;
    }

    public void incrementNumMissingOtherIndexes() {
        numMissingOtherIndexes++;
        numSchemaDiffs++;
    }

    public int getNumMissingIndexes() {
        return numMissingIndexes;
    }

    public void incrementNumMissingIndexs() {
        numMissingIndexes++;
        numSchemaDiffs++;
    }

    public DbDifference getDifference(String tableName, String tableType) {
        String key = createKey(tableType, tableName);
        DbDifference difference = (DbDifference) tables.get(key);
        if (difference == null) {
            difference = new DbDifference(tableName, tableType);
            tables.put(key, difference);
        }
        return difference;
    }

    private String createKey(String tableType, String tableName) {
        return tableType + " " + tableName;
    }

    public Database getDatabase() {
        return database;
    }

    public void setDatabase(Database database) {
        this.database = database;
    }

    public Database getOtherDatabase() {
        return otherDatabase;
    }

    public void setOtherDatabase(Database otherDatabase) {
        this.otherDatabase = otherDatabase;
    }
}
