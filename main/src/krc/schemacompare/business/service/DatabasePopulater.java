package krc.schemacompare.business.service;

import krc.schemacompare.business.domain.db.Database;

/**
 * @author mleo@kettleriverconsulting.com
 * @version $Revision$
 * @created Jul 19, 2006 9:37:20 AM
 * @cvsid $Id$
 */
public interface DatabasePopulater {
    Database getDatabase();
}
