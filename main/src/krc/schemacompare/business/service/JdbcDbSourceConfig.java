package krc.schemacompare.business.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author mleo@kettleriverconsulting.com
 * @version $Revision$
 * @created Jun 18, 2006 2:14:37 PM
 * @cvsid $Id$
 */
public class JdbcDbSourceConfig implements DbSourceConfig {

    private String i_driver = null;
    private String i_dbUrl = null;
    private String i_user = null;
    private String i_password = null;
    private String i_schema = null;
    private Connection i_connection = null;
    private String i_databaseDesignation; // the caller's "designation" for this "set of tables"

    public JdbcDbSourceConfig(String databaseDesignation,
                              String driver,
                              String dbUrl,
                              String user,
                              String password,
                              String schema) throws SQLException, ClassNotFoundException {
        i_databaseDesignation = databaseDesignation;
        i_driver = driver;
        i_dbUrl = dbUrl;
        i_user = user;
        i_password = password;
        i_schema = schema;
        connect();
    }

    private void connect() throws ClassNotFoundException, SQLException {
        Connection connection = null;
        //DriverManager.setLogWriter(new PrintWriter(System.out));
        Class.forName(getDriver());
        connection = DriverManager.getConnection(getDbUrl(),
                                          getUser(),
                                          getPassword());
        i_connection = connection;

    }

    public void close() {
        try {
            if (getConnection() != null)
                getConnection().close();
        } catch (SQLException e) {
            // ignore
        }
    }

    public String getDbUrl() {
        return i_dbUrl;
    }

    public String getDriver() {
        return i_driver;
    }

    public String getPassword() {
        return i_password;
    }

    public String getSchema() {
        return i_schema;
    }

    public String getUser() {
        return i_user;
    }

    public Connection getConnection() {
        return i_connection;
    }

    public String getDatabaseDesignation() {
        return i_databaseDesignation;
    }
}
