package krc.schemacompare.business.service;

import krc.schemacompare.business.domain.db.*;

import java.sql.*;
import java.util.Map;

/**
 * @author mleo@kettleriverconsulting.com
 * @version $Revision$
 * @created Jul 18, 2006 10:31:03 PM
 * @cvsid $Id$
 */
public class JdbcDatabasePopulater implements DatabasePopulater {

    private JdbcDbSourceConfig i_jdbcConfig;
    private Database i_database;
    private DatabaseMetaData i_databaseMetaData;

    public JdbcDatabasePopulater(JdbcDbSourceConfig jdbcConfig) throws SQLException {
        i_jdbcConfig = jdbcConfig;
        populate();
    }

    private final void populate() throws SQLException {
        Connection connection = i_jdbcConfig.getConnection();
        i_databaseMetaData = connection.getMetaData();
        i_database = new Database(i_jdbcConfig);
        populateTables();
        populateColumns();
    }

    private void populateTables() throws SQLException {
        String[] tableTypes = {"TABLE", "VIEW"};
        ResultSet rs = null;
        try {
            rs = i_databaseMetaData.getTables(null, i_jdbcConfig.getSchema(), "%", tableTypes);
            while (rs.next()) {
                String tableName = rs.getString(3);
                String tableType = rs.getString(4);
                Table table = new Table(i_database, tableName, tableType);
                i_database.addTable(table);
                if ("TABLE".equalsIgnoreCase(tableType)) {
                    populatePrimaryKeyConstraints(table);
                    populateForeignKeyConstraints(table);
                    populateIndexes(table);
                }
            }
        } finally {
            try {
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                //don't care
            }
        }
        System.out.println("Found " + i_database.getNumberOfTables() +
                " tables and views in " + i_jdbcConfig.getDatabaseDesignation());
    }

    private void populateColumns() throws SQLException {
        Map tableMap = i_database.getTableMap();
        ResultSet rs = null;
        try {
            rs = i_databaseMetaData.getColumns(null, i_jdbcConfig.getSchema(), "%", "%");
            while (rs.next()) {
                String tableName = rs.getString(3);
                Table table = (Table) tableMap.get(tableName);
                if (table == null) {
                    continue; // may have found index or system table
                }
                String columnName = rs.getString(4);
                String typeName = rs.getString(6);
                int columnSize = rs.getInt(7);
                int decimalDigits = rs.getInt(9);
                int nullable = rs.getInt(11);
                String columnDefault = rs.getString(13);
                int ordinalPosition = rs.getInt(17);
                Column Column = new Column(table,
                                           columnName,
                                           typeName,
                                           columnSize,
                                           decimalDigits,
                                           nullable,
                                           columnDefault,
                                           ordinalPosition);
                table.addColumn(Column);
            }
        } finally {
            try {
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                //don't care
            }
        }

    }

    private void populatePrimaryKeyConstraints(Table table) throws SQLException {
        ResultSet rs = null;
        try {
            rs = i_databaseMetaData.getPrimaryKeys(null, i_jdbcConfig.getSchema(), table.getTableName());
            while (rs.next()) {
                String columnName = rs.getString(4);
                short keySequence = rs.getShort(5);
                PrimaryKeyConstraint primaryKeyConstraint = new PrimaryKeyConstraint(columnName, new Short(keySequence));
                table.addPrimaryKey(primaryKeyConstraint);
            }
        } finally {
            try {
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                //don't care
            }
        }
    }

    private void populateForeignKeyConstraints(Table table) throws SQLException {
        ResultSet rs = null;
        try {
            rs = i_databaseMetaData.getImportedKeys(null, i_jdbcConfig.getSchema(), table.getTableName());
            while (rs.next()) {
                String foreignKeyName = rs.getString(12);
                Short keySequence = new Short(rs.getShort(9));
                String columnName = rs.getString(8);
                String foreignColumnName = rs.getString(4);
                String foreignTableName = rs.getString(3);
                Short deleteRule = new Short(rs.getShort(10));
                Short updateRule = new Short(rs.getShort(11));
                ForeignKeyConstraint foreignKeyConstraint =
                        new ForeignKeyConstraint(foreignKeyName,
                                                 keySequence,
                                                 columnName,
                                                 foreignColumnName,
                                                 foreignTableName,
                                                 deleteRule,
                                                 updateRule);
                table.addForeignKeyConstraint(foreignKeyConstraint);
            }
        } finally {
            try {
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                //don't care
            }
        }
    }

    private void populateIndexes(Table table) throws SQLException {
        ResultSet rs = null;
        try {
            try {
                rs = i_databaseMetaData.getIndexInfo(null, i_jdbcConfig.getSchema(), table.getTableName(), false, true);
            } catch (SQLException e) {
                System.err.println("Exception " + e.getMessage() + " thrown while querying " + i_jdbcConfig.getDatabaseDesignation() + ":" + i_jdbcConfig.getSchema() + "." + table.getTableName());
                e.printStackTrace();
                return;
            }
            while (rs.next()) {
                final String indexName = rs.getString(6);
                if (indexName == null)
                    continue; // Don't look at tableIndexStatistic columns
                final String tableName = rs.getString(3);
                final short ordinalPosition = rs.getShort(8);
                final String columnName = rs.getString(9);
                final boolean unique = rs.getBoolean(4);
                if ((tableName != null) && (columnName != null)) {
                    table.addIndexSegment(indexName,
                                          tableName,
                                          new Short(ordinalPosition),
                                          columnName,
                                          new Boolean(unique));
                }
            }
        } finally {
            try {
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                //don't care
            }
        }
    }

    public Database getDatabase() {
        return i_database;
    }

}


