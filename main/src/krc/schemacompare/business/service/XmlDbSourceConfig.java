package krc.schemacompare.business.service;

/**
 * @author mleo@kettleriverconsulting.com
 * @version $Revision$
 * @created Jul 18, 2006 10:33:12 PM
 * @cvsid $Id$
 */
public class XmlDbSourceConfig implements DbSourceConfig {

    private String i_databaseDesignation; // the caller's "designation" for this "set of tables"
    private String i_xmlFile = null;

    public XmlDbSourceConfig(String databaseDesignation, String xmlFile) {
        i_databaseDesignation = databaseDesignation;
        i_xmlFile = xmlFile;
    }

    public String getDatabaseDesignation() {
        return i_databaseDesignation;
    }

    public String getXmlFile() {
        return i_xmlFile;
    }

    public void close() {
        // nothing to do
    }
}
