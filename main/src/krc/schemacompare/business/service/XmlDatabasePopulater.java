package krc.schemacompare.business.service;

import krc.schemacompare.business.domain.db.Database;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import java.io.*;

/**
 * @author mleo@kettleriverconsulting.com
 * @version $Revision$
 * @created Jul 19, 2006 9:37:52 AM
 * @cvsid $Id$
 */
public class XmlDatabasePopulater implements DatabasePopulater {

    private XmlDbSourceConfig i_xmlConfig;
    private Database i_database;

    public XmlDatabasePopulater(XmlDbSourceConfig xmlConfig) throws IOException {
        i_xmlConfig = xmlConfig;
        populate();
    }

    private final void populate() throws IOException {

        // Slurp the XML file into a string
        String xmlFileName = i_xmlConfig.getXmlFile();
        String xmlString;
        xmlString = readFileContentsIntoString(xmlFileName);

        // Deserialize the Database object tree from the XML
        XStream xstream = new XStream(new DomDriver());
        i_database = (Database) xstream.fromXML(xmlString);

        // Re-insert the config as it is never serialized
        i_database.setDbSourceConfig(i_xmlConfig);

        System.out.println("Found " + i_database.getNumberOfTables() +
                " tables and views in " + i_xmlConfig.getDatabaseDesignation());
    }

    private String readFileContentsIntoString(String xmlFileName) throws IOException {
        String xmlString;
        byte[] data;
        File file;
        FileInputStream fis = null;
        try {
            file = new File(xmlFileName);
            fis = new FileInputStream(file);
            data = new byte[(int) file.length()];
            int r = fis.read(data);
            if (r < file.length()) {
                // deal with the short read
                throw new IllegalStateException("Couldn't entirely read the file contents of " + xmlFileName);
            }
        } finally {
            if (fis != null) {
                fis.close();
            }
        }
        xmlString = new String(data);
        return xmlString;
    }

    public Database getDatabase() {
        return i_database;
    }

}
