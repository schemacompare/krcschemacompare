package krc.schemacompare.business.service;

/**
 * @author mleo@kettleriverconsulting.com
 * @version $Revision$
 * @created Jul 19, 2006 9:33:31 AM
 * @cvsid $Id$
 */
public interface DbSourceConfig {
    String getDatabaseDesignation();
    void close();
}
