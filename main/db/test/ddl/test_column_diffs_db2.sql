create table tst_col_equal(
  bar_varchar varchar(10)
);

create table tst_col_diff_len(
  bar_varchar varchar(11)
);

create table tst_col_diff_col_name(
  bar_varchar2 varchar(10)
);

create table tst_col_diff_type(
  bar_varchar decimal(10)
);

create table tst_col_diff_null(
  bar_varchar varchar(10) null
);

create table tst_col_diff_default_value(
  bar_varchar varchar(10) default 'bob'
);

create table tst_col_diff_num_of_cols(
  bar_varchar varchar(10) default 'mal'
);

create table tst_col_diff_order_of_cols(
  bar_decimal decimal(10),
  bar_varchar varchar(10) default 'mal'
);

create table tst_col_diff_scale(
  bar_decimal decimal(10,5)
);

create table tst_col_diff_precision(
   bar_decimal decimal(20,10)
);

create table tst_col_diff_table_name2(
   bar_decimal decimal(10,10)
);