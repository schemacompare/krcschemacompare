create table tst_col_equal(
  bar_varchar varchar(10)
);

create table tst_col_diff_len(
  bar_varchar varchar(10)
);

create table tst_col_diff_col_name(
  bar_varchar varchar(10)
);

create table tst_col_diff_type(
  bar_varchar varchar(10)
);

create table tst_col_diff_null(
  bar_varchar varchar(10) not null
);

create table tst_col_diff_default_value(
  bar_varchar varchar(10) default 'mal'
);

create table tst_col_diff_num_of_cols(
  bar_varchar varchar(10) default 'mal',
  bar_decimal decimal(10)
);

create table tst_col_diff_order_of_cols(
  bar_varchar varchar(10) default 'mal',
  bar_decimal decimal(10),
  bar_int     integer
);

create table tst_col_diff_scale(
  bar_decimal decimal(10,10)
);

create table tst_col_diff_precision(
   bar_decimal decimal(10,10)
);

create table tst_col_diff_table_name(
   bar_decimal decimal(10,10)
);

create table tst_extra_table(
   bar_decimal decimal(10,10)
);