
drop table tst_idx_equal;
drop table tst_idx_diff_column;
drop table tst_idx_missing;
drop table tst_idx_mult_cols_equal;
drop table tst_idx_mult_cols_diff_order;
drop table tst_idx_mult_cols_diff_no;
drop table tst_idx_unique_same;
drop table tst_idx_unique_not_unique;
drop table tst_idx_unique_implied;

create table tst_idx_equal(
  bar_int     integer not null,
  bar_varchar varchar(10) not null default 'mal',
  bar_decimal decimal(10) not null default '1234567890'
);
create index tst_idx_equal_index ON tst_idx_equal (bar_int);

create table tst_idx_diff_column(
  bar_int     integer not null,
  bar_varchar varchar(10) not null default 'mal',
  bar_decimal decimal(10) not null default '1234567890'
);
create index tst_idx_diff_column_index ON tst_idx_diff_column (bar_varchar);

create table tst_idx_missing(
  bar_int     integer not null,
  bar_varchar varchar(10) not null default 'mal',
  bar_decimal decimal(10) not null default '1234567890'
);

create table tst_idx_mult_cols_equal(
  bar_int     integer not null,
  bar_varchar varchar(10) not null,
  bar_decimal decimal(10) not null default '1234567890'
);
create index tst_idx_mult_cols_equal_index ON tst_idx_mult_cols_equal (bar_int, bar_varchar, bar_decimal);

create table tst_idx_mult_cols_diff_order(
  bar_int     integer not null,
  bar_varchar varchar(10) not null,
  bar_decimal decimal(10) not null default '1234567890'
);
create index tst_idx_mult_cols_diff_order_index ON tst_idx_mult_cols_diff_order (bar_int, bar_decimal, bar_varchar);

create table tst_idx_mult_cols_diff_no(
  bar_int     integer not null,
  bar_varchar varchar(10) not null,
  bar_decimal decimal(10) not null default '1234567890'
);
create index tst_idx_mult_cols_diff_no_index ON tst_idx_mult_cols_diff_no (bar_int, bar_varchar);

create table tst_idx_unique_same(
  bar_int     integer not null,
  bar_varchar varchar(10) not null,
  bar_decimal decimal(10) not null default '1234567890'
);
create unique index tst_idx_unique_same_index ON tst_idx_unique_same (bar_int);

create table tst_idx_unique_not_unique(
  bar_int     integer not null,
  bar_varchar varchar(10) not null,
  bar_decimal decimal(10) not null default '1234567890'
);
create index tst_idx_unique_not_unique_index ON tst_idx_unique_not_unique (bar_int);

create table tst_idx_unique_implied(
  bar_int     integer not null UNIQUE,
  bar_varchar varchar(10) not null,
  bar_decimal decimal(10) not null default '1234567890'
);


