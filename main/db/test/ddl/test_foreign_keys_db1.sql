

create table tst_fk_parent_1(
  pkey integer not null PRIMARY KEY,
  att1 varchar(10) not null,
  att2 decimal(10) not null
);

create table tst_fk_parent_2(
  pkey1 integer not null,
  pkey2 varchar(10) not null,
  att1  decimal(10) not null,
  PRIMARY KEY (pkey1, pkey2)
);

create table tst_fk_parent_3(
  pkey integer not null PRIMARY KEY,
  att1 varchar(10) not null,
  att2 decimal(10) not null
);

create table tst_fk_equal_1table(
  mykey   integer not null PRIMARY KEY,
  myatt1  integer not null REFERENCES tst_fk_parent_1(pkey),
  myatt2  decimal(10) not null
);

create table tst_fk_equal_2cols(
  mykey   integer not null PRIMARY KEY,
  myatt1  integer not null,
  myatt2  varchar(10) not null,
  FOREIGN KEY (myatt1, myatt2) REFERENCES tst_fk_parent_2(pkey1, pkey2)
);

create table tst_fk_equal_2tables(
  mykey   integer not null PRIMARY KEY,
  myatt1  integer not null,
  myatt2  varchar(10) not null,
  myatt3  integer not null,
  FOREIGN KEY (myatt1, myatt2) REFERENCES tst_fk_parent_2(pkey1, pkey2),
  FOREIGN KEY (myatt3) REFERENCES tst_fk_parent_1(pkey)
);

create table tst_fk_missing(
  mykey   integer not null PRIMARY KEY,
  myatt1  integer not null REFERENCES tst_fk_parent_1(pkey),
  myatt2  decimal(10) not null
);

create table tst_fk_diff_table(
  mykey   integer not null PRIMARY KEY,
  myatt1  integer not null REFERENCES tst_fk_parent_1(pkey),
  myatt2  decimal(10) not null
);

create table tst_fk_one_less_col(
  mykey   integer not null PRIMARY KEY,
  myatt1  integer not null,
  myatt2  varchar(10) not null,
  FOREIGN KEY (myatt1, myatt2) REFERENCES tst_fk_parent_2(pkey1, pkey2)
);

create table tst_fk_only_one_table(
  mykey   integer not null PRIMARY KEY,
  myatt1  integer not null,
  myatt2  varchar(10) not null,
  myatt3  integer not null,
  FOREIGN KEY (myatt1, myatt2) REFERENCES tst_fk_parent_2(pkey1, pkey2),
  FOREIGN KEY (myatt3) REFERENCES tst_fk_parent_1(pkey)
);

