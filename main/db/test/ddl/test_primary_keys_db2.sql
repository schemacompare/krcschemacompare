
create table tst_pk_equal(
  bar_int     integer not null PRIMARY KEY,
  bar_varchar varchar(10) not null default 'mal',
  bar_decimal decimal(10) not null default '1234567890'
);

create table tst_pk_diff_column(
  bar_int     integer not null,
  bar_varchar varchar(10) not null default 'mal' PRIMARY KEY,
  bar_decimal decimal(10) not null default '1234567890'
);

create table tst_pk_missing(
  bar_int     integer not null,
  bar_varchar varchar(10) not null default 'mal',
  bar_decimal decimal(10) not null default '1234567890'
);

create table tst_pk_mult_cols_equal(
  bar_int     integer not null,
  bar_varchar varchar(10) not null,
  bar_decimal decimal(10) not null default '1234567890',
  PRIMARY KEY (bar_int, bar_varchar)
);

create table tst_pk_mult_cols_diff(
  bar_int     integer not null,
  bar_varchar varchar(10) not null,
  bar_decimal decimal(10) not null default '1234567890',
  PRIMARY KEY (bar_varchar, bar_int)
);

create table tst_pk_mult_cols_diff_no(
  bar_int     integer not null,
  bar_varchar varchar(10) not null,
  bar_decimal decimal(10) not null default '1234567890',
  PRIMARY KEY (bar_int, bar_varchar, bar_decimal)
);



