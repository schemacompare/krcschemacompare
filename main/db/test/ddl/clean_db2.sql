drop table tst_col_equal;
drop table tst_col_diff_len;
drop table tst_col_diff_col_name;
drop table tst_col_diff_type;
drop table tst_col_diff_null;
drop table tst_col_diff_default_value;
drop table tst_col_diff_num_of_cols;
drop table tst_col_diff_order_of_cols;
drop table tst_col_diff_scale;
drop table tst_col_diff_precision;
drop table tst_col_diff_table_name2;



drop table tst_pk_equal;
drop table tst_pk_diff_column;
drop table tst_pk_missing;
drop table tst_pk_mult_cols_equal;
drop table tst_pk_mult_cols_diff;
drop table tst_pk_mult_cols_diff_no;



drop table tst_fk_equal_1table;
drop table tst_fk_equal_2cols;
drop table tst_fk_equal_2tables;
drop table tst_fk_missing;
drop table tst_fk_diff_table;
drop table tst_fk_one_less_col;
drop table tst_fk_only_one_table;
drop table tst_fk_parent_1;
drop table tst_fk_parent_2;
drop table tst_fk_parent_3;



drop table tst_idx_equal;
drop table tst_idx_diff_column;
drop table tst_idx_missing;
drop table tst_idx_mult_cols_equal;
drop table tst_idx_mult_cols_diff_order;
drop table tst_idx_mult_cols_diff_no;
drop table tst_idx_unique_same;
drop table tst_idx_unique_not_unique;
drop table tst_idx_unique_implied;
