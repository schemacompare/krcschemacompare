# README #

This tool compares two database schemas.

### What is this repository for? ###

* Contains the source of the Schema Compare tool originally built by Kettle River Consulting Inc back in 1997, now open source
* Version 0.1

### How do I get set up? ###

* This tool is intended to be run from the command line, but could be enhanced to have a GUI
* All configuration is on the command line
* You need to supply the various database JDBC drivers

### Contribution guidelines ###

* Writing tests - Very basic tests included
* Code review - would be nice
* Other guidelines - have patience with me

### Who do I talk to? ###

* mleo@kettleriverconsulting.com
